## The following is to fix the issue "Warning: The package type's allow_virtual parameter will be changing its default value from false to true in a future release. If you do not want to allow virtual packages, please explicitly set allow_virtual to false."
Package {  allow_virtual => false, }

class basenode {
  
#  $packages=['emacs','screen', 'gnuplot', 'epel-release', 'R', 'ImageMagick']
  $packages=['emacs','screen', 'gnuplot', 'r-base', 'r-base-dev', 'imagemagick', 'xorg-x11-xauth']
  package{$packages: ensure => installed}

  $puppetscript="/root/puppet-update-and-apply.sh"
  file{$puppetscript:
    ensure => present,
    content => "#!/bin/bash
cd /home/install/labcalc/puppet
puppet apply --modulepath ./modules manifests/site.pp
",
    mode => "755",
  }
  
  cron{"runpuppet":
    hour => "*",
    minute => "*/5",
    user => "root",
    command => "$puppetscript",
    require => File[$puppetscript],
  }

  cron{"reboot":
    command => '/sbin/reboot',
    hour => 05,
    minute => 30,
    weekday => 0,
    user => 'root'
  }

}

node labcalc {

  include basenode
  include account::studenti
  include account::docenti
  include dhcp::server
  include tftp::server
  include nfs::server
  include ntp::server
  include bind::server
  include ssh::server
  include nis::server
  include repo::server
  include httpd::server
  include squid::server
  
  class {'iptables':
    profile => "server-table"
  }

  package{"gcc":
    ensure => present
  }
  
  $repopullcmd="/root/labcalc-repo-pull.sh"
  file{"$repopullcmd":
    ensure => present,
    content => "#!/bin/bash
cd /home/install/labcalc
git pull
",
    mode => 755,
  }

  # the following two cron modules open and closes the network in given periods
  cron{"opennetwork":
    hour => [12,8],
    minute => [30,30],
    user => "root",
    weekday => [1,5],
    command => "/root/wantoall.sh"
  }

  cron{"closenetwork":
    hour => [20],
    minute => [30],
    user => "root",
    weekday => [1,2,3,4,5,6,7],
    command => "/etc/rc.d/init.d/iptables restart"
  }

  cron{"repopull":
    hour => "*",
    minute => "*/10",
    user => "root",
    command => "$repopullcmd",
    require => File[$repopullcmd]
  }

  cron{"statistics":
    hour => "21",
    minute => "10",
    user => "root",
    command => "/root/collect.pl >> /root/collect.dat"
  } 

  cron{"puppet report clean":
    hour    => '*',
    minute  => '2',
    user    => 'root',
    command => 'find /var/lib/puppet/reports/labcalc.labcalc -ctime +24 -exec rm {} \;',
  }
  
  file{"/etc/resolv.conf":
    content => "search labcalc
nameserver 127.0.0.1
nameserver 151.100.4.2
nameserver 151.100.4.13
"
  }
  
  file{"/home/docenti":
    ensure => "directory"
  }

  group{"docenti":
    ensure => present,
    gid => 1100,
  }
  
  group{"studenti":
    ensure => present,
    gid => 5000,
  }

  group{"esami":
    ensure => present,
    gid => 8000,
  }

  file{"/home/studenti":
    ensure => "directory"
  }
  
  file{"/home/esami":
    ensure => "directory"
  }
  
}

node default {
  include basenode
  include nis::client
  include repo::client
  include gconf::desktop
  include arduino::client
  include openoffice
  include obsCosmPython
  
#  $packages=["firefox", "enscript", "tigervnc", "tigervnc-server", "java-1.6.0-openjdk", "xorg-x11-apps"]
  $packages=["firefox", "enscript", "tigervnc", "tigervnc-server", "software-properties-common", "oracle-java8-installer", "xorg-x11-apps"]

  package{$packages:
    ensure => present
  }

  file{"/opt/root":
    ensure => "directory"
  }  

  file{"/opt/arduino":
    ensure => "directory"
  }  

  file{"/opt/hypatia":
    ensure => "directory"
  }  

  file{"/etc/resolv.conf":
    content => "search labcalc
nameserver 192.168.1.254
"
  }

  file{"/etc/ntp.conf":
    content => "server 192.168.1.254
server 127.127.1.0
fudge 127.127.1.0 stratum 10
"
  }

  file{"/etc/hosts":
    content => "192.168.1.254 labcalc labcalc.fisica.uniroma1.it
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
"
  }
  
  file_line {"ensure mounting external software (root)":
    path => '/etc/fstab',
    line => '192.168.1.254:/opt/root /opt/root nfs defaults 0 0',
    ensure => present
  }

  file_line {"ensure mounting external software (arduino)":
    path => '/etc/fstab',
    line => '192.168.1.254:/opt/arduino /opt/arduino nfs defaults 0 0',
    ensure => present
  }

  file_line {"ensure mounting external software (hypatia for masterclass)":
    path => '/etc/fstab',
    line => '192.168.1.254:/opt/hypatia /opt/hypatia nfs defaults 0 0',
    ensure => present
  }

  service{"ntpd":
    ensure => running
  }

  service{"rpcidmapd":
    ensure => running
  }

  mount{"/home":
    atboot => true,
    device => "192.168.1.254:/home",
    ensure => "mounted",
    options => "defaults,rw",
    fstype => "nfs",
    
  }

#  cron{"shutdown":
#    command => '/sbin/shutdown -h now',
#    hour => 20,
#    minute => 0,
#    user => 'root'
#  }

  cron{"yumupdate":
    command => '/usr/bin/yum -y update > /tmp/yumupdate.log 2>&1',
    hour => 15,
    minute => 0,
    weekday => 1,
    user => 'root'
  }

  file{"/local":
    ensure => directory,
  }
  
  file{"/root/.ssh":
    ensure => directory,
  }
  
  file{"/root/.ssh/authorized_keys":
    ensure => present,
    content => "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA1NWPwRevbSSZu6zACb3EX3lVzrQm8+yETIOYvwDiYWy6EzEptlhl0MSZiotTH8kfpxxzJRjYyolaKnvqoNI6SrH0oR/d/vd5kV6amoP74vfTmJuJfefiQk/x0vAdbHTvfcBFrToM2YxABVQZMPaTsYFB4XMS6dRf3zNQxU7AcuuXhHX6ClZxq08T+vDm8TubVug7Jtctw6wNLkRW2kTgbLdIFs5TaPo1d8xcyiWP8uyqfuAdTJw5hytVk1AMtSM8ckzj8+un17cwbkfrKm+EUDfPNXv2IKcU4y4fkQ3h7sIRkRAYKAk+AHc5D5o/TRr5TFi8W8u1Kgxx0/rV29JL5Q== root@labcalc.fisica.uniroma1.it
    ",
    require => File["/root/.ssh"]
  }
  
  user{"studente":
    home => "/local/studente",
    password => "JfPBZ0gieExtQ",
    ensure => present,
    uid => 490,
    managehome => true,
    require => File["/local"]
  }
  
#  file{"/etc/rc.d/rc.local":
#    ensure => present,
#    content => "touch /var/lock/subsys/local
#chmod  o+rwx /var/lock
#chmod a+rw /dev/ttyACM0
#"
#  }

  file{"/etc/profile.d/labcalc.sh":
    ensure => present,
    content => "if [ \${XAUTHORITY} ]; then
   x0vncserver -display :0 -SecurityTypes None 1> /dev/null 2>&1 &
fi
"
  }

  file{"/etc/profile.d/labcalc.csh":
    ensure => present,
    content => "if ( \$?XAUTHORITY ) then
   x0vncserver -display :0 -SecurityTypes None 1> /dev/null 2>&1 &
endif
"
  }
}
