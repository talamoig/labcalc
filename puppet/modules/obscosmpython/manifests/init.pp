class obscosmpython {

  file{"/usr/local/lib/python2.7/site-packages/photutils-0.4.dist-info":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/photutils-0.4.dist-info",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/bz2file.pyc":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/bz2file.pyc",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/bz2file.py":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/bz2file.py",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/astropy":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/astropy",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/photutils":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/photutils",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/bz2file-0.98.dist-info":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/bz2file-0.98.dist-info",
    recurse => true
  }

  file{"/usr/local/lib/python2.7/site-packages/astropy-2.0.3.dist-info":
    source => "puppet:///modules/obscosmpython/obsCosmInstall/astropy-2.0.3.dist-info",
    recurse => true
  }

}
        
