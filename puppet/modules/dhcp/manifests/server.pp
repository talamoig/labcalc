class dhcp::server{
  $configfile="/etc/dhcp/dhcpd.conf"
  package{"dhcp": ensure => installed, allow_virtual => false,}
  file{$configfile:
    require => Package["dhcp"],
    source => "puppet:///modules/dhcp/dhcpd.conf",
    notify => Service["dhcpd"]
  }
  service{"dhcpd":
    ensure => running,
    require => [Package["dhcp"],File[$configfile]]
  }
}
