class iptables($profile){
  service{"iptables":
    ensure => running,
  }

  file{"/etc/sysconfig/iptables":
    source => "puppet:///modules/iptables/$profile",
    mode => 600,
    notify => Service["iptables"]
  }

  file{"/root/wantoall.sh":
    source => "puppet:///modules/iptables/wantoall.sh",
    mode => 755,
  }

  file{"/etc/cron.daily/closewan.sh":
    source => "puppet:///modules/iptables/closewan.sh",
    mode => 755,
  }

  cron{"iptables":
    user => root,
    hour => "1",
    minute => "0",
    command => "/etc/cron.daily/closewan.sh",
    require => File["/etc/cron.daily/closewan.sh"]
  }

  # parisi 
  cron{"parisi":
    user => root,
    hour => "09",
    minute => "45",
    weekday => "2",
    command => "/root/wantoall.sh",
    require => File["/root/wantoall.sh"]
  }

  # d'agostini
  cron{"dagostini1":
    user => root,
    hour => "10",
    minute => "45",
    weekday => "3",
    command => "/root/wantoall.sh",
    require => File["/root/wantoall.sh"]
  }
  cron{"dagostini2":
    user => root,
    hour => "13",
    minute => "45",
    weekday => "5",
    command => "/root/wantoall.sh",
    require => File["/root/wantoall.sh"]
  }

  cron{"bachelet":
    user => root,
    hour => "11",
    minute => "45",
    weekday => "1",
    command => "/root/wantoall.sh",
    require => File["/root/wantoall.sh"]
  }

}
