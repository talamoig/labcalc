#!/bin/sh

/etc/rc.d/init.d/iptables restart
/sbin/iptables -A POSTROUTING -t nat -j MASQUERADE -s 192.168.1.100 -o em1
/bin/echo 1 > /proc/sys/net/ipv4/ip_forward
