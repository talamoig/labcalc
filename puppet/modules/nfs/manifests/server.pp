class nfs::server{
  package{"nfs-utils":
    ensure => installed,
  }
  service{"rpcbind":
    ensure => running
  }
  service{"nfs":
    ensure => running,
    require => [File["/etc/exports"],Package["nfs-utils"],Service["rpcbind"]],
    subscribe => File["/etc/exports"],
  }
  
  file{"/etc/exports":
    source => "puppet:///modules/nfs/exports.$hostname"
  }
  
}
