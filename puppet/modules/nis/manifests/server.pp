class nis::server{
  $nisdomain='labcalc.fis'
  
  package{"ypserv":
    ensure => installed,
  }

  service{"ypserv":
    ensure => running,
    require => [Package['ypserv'],Exec['ypinit']],
  }

  service{"ypxfrd":
    ensure => running,
    enable => true,
    hasstatus => true,
    require => Package["ypserv"]
  }
  service{"yppasswdd":
    ensure => running,
    enable => true,
    hasstatus => true,
    require => Package["ypserv"]
  }
        
  file{"/var/yp/Makefile":
    source => "puppet:///modules/nis/Makefile",
    ensure => present
  }
  
  file{"/var/yp/securenets":
    content => "host 127.0.0.1
255.255.255.0 192.168.1.0",
    require => Package["ypserv"],
    notify => Service["ypserv"]
  }

  exec{"domainconfig":
    command => "/bin/echo $nisdomain >> /etc/sysconfig/network",
    unless => "/bin/grep $nisdomain /etc/sysconfig/network",
  }
  
  exec{"ypinit":
    command => "/bin/ypdomainname $nisdomain; /usr/lib64/yp/ypinit -m",
    unless => "/bin/ls /var/yp/$nisdomain",
    require => [Package["ypserv"],File['/var/yp/securenets']]
  }
  exec{"nisupdate":
    cwd => '/var/yp',
    command => "/usr/bin/make",
    refreshonly => true,
    require => File["/var/yp/Makefile"]
  }
  
}
