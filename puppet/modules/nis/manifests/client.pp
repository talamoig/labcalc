class nis::client{
  package {"yp-tools":
    ensure => installed,
  }
  
  package{"ypbind":
    ensure => installed,
  }

  service{"ypbind":
    ensure => running,
    enable => true,
    hasstatus => true,
    require => [Package["ypbind"],Exec["nisconfig"]]
  }
  
  exec{"nisconfig":
    command => "/usr/sbin/authconfig --enablemd5 --nisdomain=labcalc.fis --nisserver=server --enablecache --enablenis --update",
    unless => "/usr/bin/ypwhich",
    require => [Package['yp-tools'],Package['ypbind']]
  }

  file_line { "Append a line to passwd":
    path => '/etc/passwd',
    line => '+::::::'
  }
}
