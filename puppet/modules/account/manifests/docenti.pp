class account::docenti{
##  account::useraccount{"Nome Cognome": kind => "docenti", login=>"username", uid => 110x}
##  from a shell you can use something similar to the following to generate a set of students group accounts. choose carefully the value for uid!
##  for i in `seq -w 1 50`; do echo "account::useraccount{'lcsr$i': kind => 'studenti', login=>'lcsr$i', uid => `expr 5080 + $i`, passwd => True}"; done

account::useraccount{"Ivano Talamo": kind => "docenti", login=>"talamoig", uid => 500 }
account::useraccount{"Luciano Barone": kind => "docenti", login=>"barone", uid => 1100 }
account::useraccount{"Andrea Maiorano": kind => "docenti", login=>"maiorano", uid => 1101}
account::useraccount{"Shahram Rahatlou": kind => "docenti", login=>"rahatlou", uid => 1102}
account::useraccount{"Riccardo Faccini": kind => "docenti", login=>"faccini", uid => 1103}
account::useraccount{"Giovanni Organtini": kind => "docenti", login=>"organtin", uid => 1104}
account::useraccount{"Marco Limongi": kind => "docenti", login=>"limongi", uid => 1105}
account::useraccount{"Stefano Veneziano": kind => "docenti", login=>"stefanov", uid => 1106}
account::useraccount{"Giovanni Bachelet": kind => "docenti", login=>"bachelet", uid => 1107}
account::useraccount{"Federico Ricci-Tersenghi": kind => "docenti", login=>"riccife", uid => 1108}
account::useraccount{"Enzo Marinari": kind => "docenti", login=>"marinari", uid => 1109}
account::useraccount{"Francesco Mauri": kind => "docenti", login=>"mauri", uid => 1110}
account::useraccount{"Paolo Pani": kind => "docenti", login=>"pani", uid => 1111}
account::useraccount{"Massimiliano Viale": kind => "docenti", login=>"viale", uid => 1112}
account::useraccount{"Badder Marzocchi": kind => "docenti", login=>"marzocchi", uid => 1113}
account::useraccount{"Elisabetta Falivene": kind => "docenti", login=>"falivene", uid => 1114}
account::useraccount{"Miguel Ibanez": kind => "docenti", login=>"ibanez", uid => 1115}
account::useraccount{"Lilia Boeri": kind => "docenti", login=>"boeri", uid => 1116}
account::useraccount{"Riccardo Paramatti": kind => "docenti", login=>"paramatti", uid => 1117}
account::useraccount{"Maria Chiara Angelini": kind => "docenti", login=>"mcangeli", uid => 1118}
account::useraccount{"Cristiano De Michele": kind => "docenti", login=>"demichele", uid => 1119}
account::useraccount{"Luca Leuzzi": kind => "docenti", login=>"leuzzi", uid => 1120}
account::useraccount{"Giulio D'Agostini": kind => "docenti", login=>"dagos", uid => 1121}
account::useraccount{"Lorenzo Rovigatti": kind => "docenti", login=>"lorenzo", uid => 1122}
account::useraccount{"Nicoletta Gnan": kind => "docenti", login=>"ngnan", uid => 1123}
account::useraccount{"Livia Soffi": kind => "docenti", login=>"soffi", uid => 1124}
account::useraccount{"Saverio Moroni": kind => "docenti", login=>"moroni", uid => 1125}
account::useraccount{"Marco Vignati": kind => "docenti", login=>"vignati", uid => 1126}
account::useraccount{"Enzo Pascale": kind => "docenti", login=>"pascale", uid => 1127}  
}
