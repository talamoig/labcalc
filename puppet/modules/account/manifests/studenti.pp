class account::studenti{
  ## studenti observational cosmology
    account::useraccount{'obscosm01': kind => 'studenti', login=>'obscosm01', uid => 9501, passwd => True}
    account::useraccount{'obscosm02': kind => 'studenti', login=>'obscosm02', uid => 9502, passwd => True}
    account::useraccount{'obscosm03': kind => 'studenti', login=>'obscosm03', uid => 9503, passwd => True}
    account::useraccount{'obscosm04': kind => 'studenti', login=>'obscosm04', uid => 9504, passwd => True}
    account::useraccount{'obscosm05': kind => 'studenti', login=>'obscosm05', uid => 9505, passwd => True}
    account::useraccount{'obscosm06': kind => 'studenti', login=>'obscosm06', uid => 9506, passwd => True}
    account::useraccount{'obscosm07': kind => 'studenti', login=>'obscosm07', uid => 9507, passwd => True}
    account::useraccount{'obscosm08': kind => 'studenti', login=>'obscosm08', uid => 9508, passwd => True}
    account::useraccount{'obscosm09': kind => 'studenti', login=>'obscosm09', uid => 9509, passwd => True}
    account::useraccount{'obscosm10': kind => 'studenti', login=>'obscosm10', uid => 9510, passwd => True}
    account::useraccount{'obscosm11': kind => 'studenti', login=>'obscosm11', uid => 9511, passwd => True}
    account::useraccount{'obscosm12': kind => 'studenti', login=>'obscosm12', uid => 9512, passwd => True}
    account::useraccount{'obscosm13': kind => 'studenti', login=>'obscosm13', uid => 9513, passwd => True}
    account::useraccount{'obscosm14': kind => 'studenti', login=>'obscosm14', uid => 9514, passwd => True}
    account::useraccount{'obscosm15': kind => 'studenti', login=>'obscosm15', uid => 9515, passwd => True}
    account::useraccount{'obscosm16': kind => 'studenti', login=>'obscosm16', uid => 9516, passwd => True}
    account::useraccount{'obscosm17': kind => 'studenti', login=>'obscosm17', uid => 9517, passwd => True}
    account::useraccount{'obscosm18': kind => 'studenti', login=>'obscosm18', uid => 9518, passwd => True}
    account::useraccount{'obscosm19': kind => 'studenti', login=>'obscosm19', uid => 9519, passwd => True}
    account::useraccount{'obscosm20': kind => 'studenti', login=>'obscosm20', uid => 9520, passwd => True}
  ## studenti Laboratori Loreto
    account::useraccount{'sdl_exp01': kind => 'studenti', login=>'sdl_exp01', uid => 7001, passwd => True}
    account::useraccount{'sdl_exp02': kind => 'studenti', login=>'sdl_exp02', uid => 7002, passwd => True}
    account::useraccount{'sdl_exp03': kind => 'studenti', login=>'sdl_exp03', uid => 7003, passwd => True}
    account::useraccount{'sdl_exp04': kind => 'studenti', login=>'sdl_exp04', uid => 7004, passwd => True}
    account::useraccount{'sdl_exp05': kind => 'studenti', login=>'sdl_exp05', uid => 7005, passwd => True}
    account::useraccount{'sdl_exp06': kind => 'studenti', login=>'sdl_exp06', uid => 7006, passwd => True}
    account::useraccount{'sdl_exp07': kind => 'studenti', login=>'sdl_exp07', uid => 7007, passwd => True}
    account::useraccount{'sdl_exp08': kind => 'studenti', login=>'sdl_exp08', uid => 7008, passwd => True}
    account::useraccount{'sdl_exp09': kind => 'studenti', login=>'sdl_exp09', uid => 7009, passwd => True}
    account::useraccount{'sdl_exp10': kind => 'studenti', login=>'sdl_exp10', uid => 7010, passwd => True}
    account::useraccount{'sdl_exp11': kind => 'studenti', login=>'sdl_exp11', uid => 7011, passwd => True}
    account::useraccount{'sdl_exp12': kind => 'studenti', login=>'sdl_exp12', uid => 7012, passwd => True}
    account::useraccount{'sdl_exp13': kind => 'studenti', login=>'sdl_exp13', uid => 7013, passwd => True}
    account::useraccount{'sdl_exp14': kind => 'studenti', login=>'sdl_exp14', uid => 7014, passwd => True}
    account::useraccount{'sdl_exp15': kind => 'studenti', login=>'sdl_exp15', uid => 7015, passwd => True}
    account::useraccount{'sdl_exp16': kind => 'studenti', login=>'sdl_exp16', uid => 7016, passwd => True}
    account::useraccount{'sdl_exp17': kind => 'studenti', login=>'sdl_exp17', uid => 7017, passwd => True}
    account::useraccount{'sdl_exp18': kind => 'studenti', login=>'sdl_exp18', uid => 7018, passwd => True}
    account::useraccount{'sdl_exp19': kind => 'studenti', login=>'sdl_exp19', uid => 7019, passwd => True}
    account::useraccount{'sdl_exp20': kind => 'studenti', login=>'sdl_exp20', uid => 7020, passwd => True}

  ## studenti di Marco Limongi
    account::useraccount{'lca01': kind => 'studenti', login=>'lca01', uid => 5001, passwd => True}
    account::useraccount{'lca02': kind => 'studenti', login=>'lca02', uid => 5002, passwd => True}
    account::useraccount{'lca03': kind => 'studenti', login=>'lca03', uid => 5003, passwd => True}
    account::useraccount{'lca04': kind => 'studenti', login=>'lca04', uid => 5004, passwd => True}
    account::useraccount{'lca05': kind => 'studenti', login=>'lca05', uid => 5005, passwd => True}
    account::useraccount{'lca06': kind => 'studenti', login=>'lca06', uid => 5006, passwd => True}
    account::useraccount{'lca07': kind => 'studenti', login=>'lca07', uid => 5007, passwd => True}
    account::useraccount{'lca08': kind => 'studenti', login=>'lca08', uid => 5008, passwd => True}
    account::useraccount{'lca09': kind => 'studenti', login=>'lca09', uid => 5009, passwd => True}
    account::useraccount{'lca10': kind => 'studenti', login=>'lca10', uid => 5010, passwd => True}
    account::useraccount{'lca11': kind => 'studenti', login=>'lca11', uid => 5011, passwd => True}
    account::useraccount{'lca12': kind => 'studenti', login=>'lca12', uid => 5012, passwd => True}
    account::useraccount{'lca13': kind => 'studenti', login=>'lca13', uid => 5013, passwd => True}
    account::useraccount{'lca14': kind => 'studenti', login=>'lca14', uid => 5014, passwd => True}
    account::useraccount{'lca15': kind => 'studenti', login=>'lca15', uid => 5015, passwd => True}
    account::useraccount{'lca16': kind => 'studenti', login=>'lca16', uid => 5016, passwd => True}
    account::useraccount{'lca17': kind => 'studenti', login=>'lca17', uid => 5017, passwd => True}
    account::useraccount{'lca18': kind => 'studenti', login=>'lca18', uid => 5018, passwd => True}
    account::useraccount{'lca19': kind => 'studenti', login=>'lca19', uid => 5019, passwd => True}
    account::useraccount{'lca20': kind => 'studenti', login=>'lca20', uid => 5020, passwd => True}
    account::useraccount{'lca21': kind => 'studenti', login=>'lca21', uid => 5021, passwd => True}
    account::useraccount{'lca22': kind => 'studenti', login=>'lca22', uid => 5022, passwd => True}
    account::useraccount{'lca23': kind => 'studenti', login=>'lca23', uid => 5023, passwd => True}
    account::useraccount{'lca24': kind => 'studenti', login=>'lca24', uid => 5024, passwd => True}
    account::useraccount{'lca25': kind => 'studenti', login=>'lca25', uid => 5025, passwd => True}
    account::useraccount{'lca26': kind => 'studenti', login=>'lca26', uid => 5026, passwd => True}
    account::useraccount{'lca27': kind => 'studenti', login=>'lca27', uid => 5027, passwd => True}
    account::useraccount{'lca28': kind => 'studenti', login=>'lca28', uid => 5028, passwd => True}
    account::useraccount{'lca29': kind => 'studenti', login=>'lca29', uid => 5029, passwd => True}
    account::useraccount{'lca30': kind => 'studenti', login=>'lca30', uid => 5030, passwd => True}
    account::useraccount{'lca31': kind => 'studenti', login=>'lca31', uid => 6482, passwd => True}
    account::useraccount{'lca32': kind => 'studenti', login=>'lca32', uid => 6483, passwd => True}
    account::useraccount{'lca33': kind => 'studenti', login=>'lca33', uid => 6484, passwd => True}
    account::useraccount{'lca34': kind => 'studenti', login=>'lca34', uid => 6485, passwd => True}
    account::useraccount{'lca35': kind => 'studenti', login=>'lca35', uid => 6486, passwd => True}
    account::useraccount{'lca36': kind => 'studenti', login=>'lca36', uid => 6487, passwd => True}
    account::useraccount{'lca37': kind => 'studenti', login=>'lca37', uid => 6488, passwd => True}
    account::useraccount{'lca38': kind => 'studenti', login=>'lca38', uid => 6489, passwd => True}
    account::useraccount{'lca39': kind => 'studenti', login=>'lca39', uid => 6490, passwd => True}
    account::useraccount{'lca40': kind => 'studenti', login=>'lca40', uid => 6491, passwd => True}

  ## studenti di Riccardo Faccini
    account::useraccount{'lcrf01': kind => 'studenti', login=>'lcrf01', uid => 5031, passwd => True}
    account::useraccount{'lcrf02': kind => 'studenti', login=>'lcrf02', uid => 5032, passwd => True}
    account::useraccount{'lcrf03': kind => 'studenti', login=>'lcrf03', uid => 5033, passwd => True}
    account::useraccount{'lcrf04': kind => 'studenti', login=>'lcrf04', uid => 5034, passwd => True}
    account::useraccount{'lcrf05': kind => 'studenti', login=>'lcrf05', uid => 5035, passwd => True}
    account::useraccount{'lcrf06': kind => 'studenti', login=>'lcrf06', uid => 5036, passwd => True}
    account::useraccount{'lcrf07': kind => 'studenti', login=>'lcrf07', uid => 5037, passwd => True}
    account::useraccount{'lcrf08': kind => 'studenti', login=>'lcrf08', uid => 5038, passwd => True}
    account::useraccount{'lcrf09': kind => 'studenti', login=>'lcrf09', uid => 5039, passwd => True}
    account::useraccount{'lcrf10': kind => 'studenti', login=>'lcrf10', uid => 5040, passwd => True}
    account::useraccount{'lcrf11': kind => 'studenti', login=>'lcrf11', uid => 5041, passwd => True}
    account::useraccount{'lcrf12': kind => 'studenti', login=>'lcrf12', uid => 5042, passwd => True}
    account::useraccount{'lcrf13': kind => 'studenti', login=>'lcrf13', uid => 5043, passwd => True}
    account::useraccount{'lcrf14': kind => 'studenti', login=>'lcrf14', uid => 5044, passwd => True}
    account::useraccount{'lcrf15': kind => 'studenti', login=>'lcrf15', uid => 5045, passwd => True}
    account::useraccount{'lcrf16': kind => 'studenti', login=>'lcrf16', uid => 5046, passwd => True}
    account::useraccount{'lcrf17': kind => 'studenti', login=>'lcrf17', uid => 5047, passwd => True}
    account::useraccount{'lcrf18': kind => 'studenti', login=>'lcrf18', uid => 5048, passwd => True}
    account::useraccount{'lcrf19': kind => 'studenti', login=>'lcrf19', uid => 5049, passwd => True}
    account::useraccount{'lcrf20': kind => 'studenti', login=>'lcrf20', uid => 5050, passwd => True}
    account::useraccount{'lcrf21': kind => 'studenti', login=>'lcrf21', uid => 5051, passwd => True}
    account::useraccount{'lcrf22': kind => 'studenti', login=>'lcrf22', uid => 5052, passwd => True}
    account::useraccount{'lcrf23': kind => 'studenti', login=>'lcrf23', uid => 5053, passwd => True}
    account::useraccount{'lcrf24': kind => 'studenti', login=>'lcrf24', uid => 5054, passwd => True}
    account::useraccount{'lcrf25': kind => 'studenti', login=>'lcrf25', uid => 5055, passwd => True}
    account::useraccount{'lcrf26': kind => 'studenti', login=>'lcrf26', uid => 5056, passwd => True}
    account::useraccount{'lcrf27': kind => 'studenti', login=>'lcrf27', uid => 5057, passwd => True}
    account::useraccount{'lcrf28': kind => 'studenti', login=>'lcrf28', uid => 5058, passwd => True}
    account::useraccount{'lcrf29': kind => 'studenti', login=>'lcrf29', uid => 5059, passwd => True}
    account::useraccount{'lcrf30': kind => 'studenti', login=>'lcrf30', uid => 5060, passwd => True}
    account::useraccount{'lcrf31': kind => 'studenti', login=>'lcrf31', uid => 5061, passwd => True}
    account::useraccount{'lcrf32': kind => 'studenti', login=>'lcrf32', uid => 5062, passwd => True}
    account::useraccount{'lcrf33': kind => 'studenti', login=>'lcrf33', uid => 5063, passwd => True}
    account::useraccount{'lcrf34': kind => 'studenti', login=>'lcrf34', uid => 5064, passwd => True}
    account::useraccount{'lcrf35': kind => 'studenti', login=>'lcrf35', uid => 5065, passwd => True}
    account::useraccount{'lcrf36': kind => 'studenti', login=>'lcrf36', uid => 5066, passwd => True}
    account::useraccount{'lcrf37': kind => 'studenti', login=>'lcrf37', uid => 5067, passwd => True}
    account::useraccount{'lcrf38': kind => 'studenti', login=>'lcrf38', uid => 5068, passwd => True}
    account::useraccount{'lcrf39': kind => 'studenti', login=>'lcrf39', uid => 5069, passwd => True}
    account::useraccount{'lcrf40': kind => 'studenti', login=>'lcrf40', uid => 5070, passwd => True}
    account::useraccount{'lcrf41': kind => 'studenti', login=>'lcrf41', uid => 5071, passwd => True}
    account::useraccount{'lcrf42': kind => 'studenti', login=>'lcrf42', uid => 5072, passwd => True}
    account::useraccount{'lcrf43': kind => 'studenti', login=>'lcrf43', uid => 5073, passwd => True}
    account::useraccount{'lcrf44': kind => 'studenti', login=>'lcrf44', uid => 5074, passwd => True}
    account::useraccount{'lcrf45': kind => 'studenti', login=>'lcrf45', uid => 5075, passwd => True}
    account::useraccount{'lcrf46': kind => 'studenti', login=>'lcrf46', uid => 5076, passwd => True}
    account::useraccount{'lcrf47': kind => 'studenti', login=>'lcrf47', uid => 5077, passwd => True}
    account::useraccount{'lcrf48': kind => 'studenti', login=>'lcrf48', uid => 5078, passwd => True}
    account::useraccount{'lcrf49': kind => 'studenti', login=>'lcrf49', uid => 5079, passwd => True}
    account::useraccount{'lcrf50': kind => 'studenti', login=>'lcrf50', uid => 5080, passwd => True}
    account::useraccount{'lcrf51': kind => 'studenti', login=>'lcrf51', uid => 5231, passwd => True}
    account::useraccount{'lcrf52': kind => 'studenti', login=>'lcrf52', uid => 5232, passwd => True}
    account::useraccount{'lcrf53': kind => 'studenti', login=>'lcrf53', uid => 5233, passwd => True}
    account::useraccount{'lcrf54': kind => 'studenti', login=>'lcrf54', uid => 5234, passwd => True}
    account::useraccount{'lcrf55': kind => 'studenti', login=>'lcrf55', uid => 5235, passwd => True}
    account::useraccount{'lcrf56': kind => 'studenti', login=>'lcrf56', uid => 5236, passwd => True}
    account::useraccount{'lcrf57': kind => 'studenti', login=>'lcrf57', uid => 5237, passwd => True}
    account::useraccount{'lcrf58': kind => 'studenti', login=>'lcrf58', uid => 5238, passwd => True}
    account::useraccount{'lcrf59': kind => 'studenti', login=>'lcrf59', uid => 5239, passwd => True}
    account::useraccount{'lcrf60': kind => 'studenti', login=>'lcrf60', uid => 5240, passwd => True}
    account::useraccount{'lcrf61': kind => 'studenti', login=>'lcrf61', uid => 5241, passwd => True}
    account::useraccount{'lcrf62': kind => 'studenti', login=>'lcrf62', uid => 5242, passwd => True}
    account::useraccount{'lcrf63': kind => 'studenti', login=>'lcrf63', uid => 5243, passwd => True}
    account::useraccount{'lcrf64': kind => 'studenti', login=>'lcrf64', uid => 5244, passwd => True}
    account::useraccount{'lcrf65': kind => 'studenti', login=>'lcrf65', uid => 5245, passwd => True}
    account::useraccount{'lcrf66': kind => 'studenti', login=>'lcrf66', uid => 5246, passwd => True}
    account::useraccount{'lcrf67': kind => 'studenti', login=>'lcrf67', uid => 5247, passwd => True}
    account::useraccount{'lcrf68': kind => 'studenti', login=>'lcrf68', uid => 5248, passwd => True}
    account::useraccount{'lcrf69': kind => 'studenti', login=>'lcrf69', uid => 5249, passwd => True}
    account::useraccount{'lcrf70': kind => 'studenti', login=>'lcrf70', uid => 5250, passwd => True}
    account::useraccount{'lcrf71': kind => 'studenti', login=>'lcrf71', uid => 5251, passwd => True}
    account::useraccount{'lcrf72': kind => 'studenti', login=>'lcrf72', uid => 5252, passwd => True}
    account::useraccount{'lcrf73': kind => 'studenti', login=>'lcrf73', uid => 5253, passwd => True}
    account::useraccount{'lcrf74': kind => 'studenti', login=>'lcrf74', uid => 5254, passwd => True}
    account::useraccount{'lcrf75': kind => 'studenti', login=>'lcrf75', uid => 5255, passwd => True}
    account::useraccount{'lcrf76': kind => 'studenti', login=>'lcrf76', uid => 5256, passwd => True}
    account::useraccount{'lcrf77': kind => 'studenti', login=>'lcrf77', uid => 5257, passwd => True}
    account::useraccount{'lcrf78': kind => 'studenti', login=>'lcrf78', uid => 5258, passwd => True}
    account::useraccount{'lcrf79': kind => 'studenti', login=>'lcrf79', uid => 5259, passwd => True}
    account::useraccount{'lcrf80': kind => 'studenti', login=>'lcrf80', uid => 5260, passwd => True}

  ## studenti di Lilia Boeri
    account::useraccount{'lcbl01': kind => 'studenti', login=>'lcbl01', uid => 5081, passwd => True}
    account::useraccount{'lcbl02': kind => 'studenti', login=>'lcbl02', uid => 5082, passwd => True}
    account::useraccount{'lcbl03': kind => 'studenti', login=>'lcbl03', uid => 5083, passwd => True}
    account::useraccount{'lcbl04': kind => 'studenti', login=>'lcbl04', uid => 5084, passwd => True}
    account::useraccount{'lcbl05': kind => 'studenti', login=>'lcbl05', uid => 5085, passwd => True}
    account::useraccount{'lcbl06': kind => 'studenti', login=>'lcbl06', uid => 5086, passwd => True}
    account::useraccount{'lcbl07': kind => 'studenti', login=>'lcbl07', uid => 5087, passwd => True}
    account::useraccount{'lcbl08': kind => 'studenti', login=>'lcbl08', uid => 5088, passwd => True}
    account::useraccount{'lcbl09': kind => 'studenti', login=>'lcbl09', uid => 5089, passwd => True}
    account::useraccount{'lcbl10': kind => 'studenti', login=>'lcbl10', uid => 5090, passwd => True}
    account::useraccount{'lcbl11': kind => 'studenti', login=>'lcbl11', uid => 5091, passwd => True}
    account::useraccount{'lcbl12': kind => 'studenti', login=>'lcbl12', uid => 5092, passwd => True}
    account::useraccount{'lcbl13': kind => 'studenti', login=>'lcbl13', uid => 5093, passwd => True}
    account::useraccount{'lcbl14': kind => 'studenti', login=>'lcbl14', uid => 5094, passwd => True}
    account::useraccount{'lcbl15': kind => 'studenti', login=>'lcbl15', uid => 5095, passwd => True}
    account::useraccount{'lcbl16': kind => 'studenti', login=>'lcbl16', uid => 5096, passwd => True}
    account::useraccount{'lcbl17': kind => 'studenti', login=>'lcbl17', uid => 5097, passwd => True}
    account::useraccount{'lcbl18': kind => 'studenti', login=>'lcbl18', uid => 5098, passwd => True}
    account::useraccount{'lcbl19': kind => 'studenti', login=>'lcbl19', uid => 5099, passwd => True}
    account::useraccount{'lcbl20': kind => 'studenti', login=>'lcbl20', uid => 5100, passwd => True}
    account::useraccount{'lcbl21': kind => 'studenti', login=>'lcbl21', uid => 5101, passwd => True}
    account::useraccount{'lcbl22': kind => 'studenti', login=>'lcbl22', uid => 5102, passwd => True}
    account::useraccount{'lcbl23': kind => 'studenti', login=>'lcbl23', uid => 5103, passwd => True}
    account::useraccount{'lcbl24': kind => 'studenti', login=>'lcbl24', uid => 5104, passwd => True}
    account::useraccount{'lcbl25': kind => 'studenti', login=>'lcbl25', uid => 5105, passwd => True}
    account::useraccount{'lcbl26': kind => 'studenti', login=>'lcbl26', uid => 5106, passwd => True}
    account::useraccount{'lcbl27': kind => 'studenti', login=>'lcbl27', uid => 5107, passwd => True}
    account::useraccount{'lcbl28': kind => 'studenti', login=>'lcbl28', uid => 5108, passwd => True}
    account::useraccount{'lcbl29': kind => 'studenti', login=>'lcbl29', uid => 5109, passwd => True}
    account::useraccount{'lcbl30': kind => 'studenti', login=>'lcbl30', uid => 5110, passwd => True}
    account::useraccount{'lcbl31': kind => 'studenti', login=>'lcbl31', uid => 5111, passwd => True}
    account::useraccount{'lcbl32': kind => 'studenti', login=>'lcbl32', uid => 5112, passwd => True}
    account::useraccount{'lcbl33': kind => 'studenti', login=>'lcbl33', uid => 5113, passwd => True}
    account::useraccount{'lcbl34': kind => 'studenti', login=>'lcbl34', uid => 5114, passwd => True}
    account::useraccount{'lcbl35': kind => 'studenti', login=>'lcbl35', uid => 5115, passwd => True}
    account::useraccount{'lcbl36': kind => 'studenti', login=>'lcbl36', uid => 5116, passwd => True}
    account::useraccount{'lcbl37': kind => 'studenti', login=>'lcbl37', uid => 5117, passwd => True}
    account::useraccount{'lcbl38': kind => 'studenti', login=>'lcbl38', uid => 5118, passwd => True}
    account::useraccount{'lcbl39': kind => 'studenti', login=>'lcbl39', uid => 5119, passwd => True}
    account::useraccount{'lcbl40': kind => 'studenti', login=>'lcbl40', uid => 5120, passwd => True}
    account::useraccount{'lcbl41': kind => 'studenti', login=>'lcbl41', uid => 5121, passwd => True}
    account::useraccount{'lcbl42': kind => 'studenti', login=>'lcbl42', uid => 5122, passwd => True}
    account::useraccount{'lcbl43': kind => 'studenti', login=>'lcbl43', uid => 5123, passwd => True}
    account::useraccount{'lcbl44': kind => 'studenti', login=>'lcbl44', uid => 5124, passwd => True}
    account::useraccount{'lcbl45': kind => 'studenti', login=>'lcbl45', uid => 5125, passwd => True}
    account::useraccount{'lcbl46': kind => 'studenti', login=>'lcbl46', uid => 5126, passwd => True}
    account::useraccount{'lcbl47': kind => 'studenti', login=>'lcbl47', uid => 5127, passwd => True}
    account::useraccount{'lcbl48': kind => 'studenti', login=>'lcbl48', uid => 5128, passwd => True}
    account::useraccount{'lcbl49': kind => 'studenti', login=>'lcbl49', uid => 5129, passwd => True}
    account::useraccount{'lcbl50': kind => 'studenti', login=>'lcbl50', uid => 5130, passwd => True}
    account::useraccount{'lcbl51': kind => 'studenti', login=>'lcbl51', uid => 6521, passwd => True}
    account::useraccount{'lcbl52': kind => 'studenti', login=>'lcbl52', uid => 6522, passwd => True}
    account::useraccount{'lcbl53': kind => 'studenti', login=>'lcbl53', uid => 6523, passwd => True}
    account::useraccount{'lcbl54': kind => 'studenti', login=>'lcbl54', uid => 6524, passwd => True}
    account::useraccount{'lcbl55': kind => 'studenti', login=>'lcbl55', uid => 6525, passwd => True}
    account::useraccount{'lcbl56': kind => 'studenti', login=>'lcbl56', uid => 6526, passwd => True}
    account::useraccount{'lcbl57': kind => 'studenti', login=>'lcbl57', uid => 6527, passwd => True}
    account::useraccount{'lcbl58': kind => 'studenti', login=>'lcbl58', uid => 6528, passwd => True}
    account::useraccount{'lcbl59': kind => 'studenti', login=>'lcbl59', uid => 6529, passwd => True}
    account::useraccount{'lcbl60': kind => 'studenti', login=>'lcbl60', uid => 6530, passwd => True}
    account::useraccount{'lcbl61': kind => 'studenti', login=>'lcbl61', uid => 6582, passwd => True}
    account::useraccount{'lcbl62': kind => 'studenti', login=>'lcbl62', uid => 6583, passwd => True}
    account::useraccount{'lcbl63': kind => 'studenti', login=>'lcbl63', uid => 6584, passwd => True}
    account::useraccount{'lcbl64': kind => 'studenti', login=>'lcbl64', uid => 6585, passwd => True}
  ## studenti di Giovanni Organtini
    account::useraccount{'lcgo01': kind => 'studenti', login=>'lcgo01', uid => 5131, passwd => True}
    account::useraccount{'lcgo02': kind => 'studenti', login=>'lcgo02', uid => 5132, passwd => True}
    account::useraccount{'lcgo03': kind => 'studenti', login=>'lcgo03', uid => 5133, passwd => True}
    account::useraccount{'lcgo04': kind => 'studenti', login=>'lcgo04', uid => 5134, passwd => True}
    account::useraccount{'lcgo05': kind => 'studenti', login=>'lcgo05', uid => 5135, passwd => True}
    account::useraccount{'lcgo06': kind => 'studenti', login=>'lcgo06', uid => 5136, passwd => True}
    account::useraccount{'lcgo07': kind => 'studenti', login=>'lcgo07', uid => 5137, passwd => True}
    account::useraccount{'lcgo08': kind => 'studenti', login=>'lcgo08', uid => 5138, passwd => True}
    account::useraccount{'lcgo09': kind => 'studenti', login=>'lcgo09', uid => 5139, passwd => True}
    account::useraccount{'lcgo10': kind => 'studenti', login=>'lcgo10', uid => 5140, passwd => True}
    account::useraccount{'lcgo11': kind => 'studenti', login=>'lcgo11', uid => 5141, passwd => True}
    account::useraccount{'lcgo12': kind => 'studenti', login=>'lcgo12', uid => 5142, passwd => True}
    account::useraccount{'lcgo13': kind => 'studenti', login=>'lcgo13', uid => 5143, passwd => True}
    account::useraccount{'lcgo14': kind => 'studenti', login=>'lcgo14', uid => 5144, passwd => True}
    account::useraccount{'lcgo15': kind => 'studenti', login=>'lcgo15', uid => 5145, passwd => True}
    account::useraccount{'lcgo16': kind => 'studenti', login=>'lcgo16', uid => 5146, passwd => True}
    account::useraccount{'lcgo17': kind => 'studenti', login=>'lcgo17', uid => 5147, passwd => True}
    account::useraccount{'lcgo18': kind => 'studenti', login=>'lcgo18', uid => 5148, passwd => True}
    account::useraccount{'lcgo19': kind => 'studenti', login=>'lcgo19', uid => 5149, passwd => True}
    account::useraccount{'lcgo20': kind => 'studenti', login=>'lcgo20', uid => 5150, passwd => True}
    account::useraccount{'lcgo21': kind => 'studenti', login=>'lcgo21', uid => 5151, passwd => True}
    account::useraccount{'lcgo22': kind => 'studenti', login=>'lcgo22', uid => 5152, passwd => True}
    account::useraccount{'lcgo23': kind => 'studenti', login=>'lcgo23', uid => 5153, passwd => True}
    account::useraccount{'lcgo24': kind => 'studenti', login=>'lcgo24', uid => 5154, passwd => True}
    account::useraccount{'lcgo25': kind => 'studenti', login=>'lcgo25', uid => 5155, passwd => True}
    account::useraccount{'lcgo26': kind => 'studenti', login=>'lcgo26', uid => 5156, passwd => True}
    account::useraccount{'lcgo27': kind => 'studenti', login=>'lcgo27', uid => 5157, passwd => True}
    account::useraccount{'lcgo28': kind => 'studenti', login=>'lcgo28', uid => 5158, passwd => True}
    account::useraccount{'lcgo29': kind => 'studenti', login=>'lcgo29', uid => 5159, passwd => True}
    account::useraccount{'lcgo30': kind => 'studenti', login=>'lcgo30', uid => 5160, passwd => True}
    account::useraccount{'lcgo31': kind => 'studenti', login=>'lcgo31', uid => 5161, passwd => True}
    account::useraccount{'lcgo32': kind => 'studenti', login=>'lcgo32', uid => 5162, passwd => True}
    account::useraccount{'lcgo33': kind => 'studenti', login=>'lcgo33', uid => 5163, passwd => True}
    account::useraccount{'lcgo34': kind => 'studenti', login=>'lcgo34', uid => 5164, passwd => True}
    account::useraccount{'lcgo35': kind => 'studenti', login=>'lcgo35', uid => 5165, passwd => True}
    account::useraccount{'lcgo36': kind => 'studenti', login=>'lcgo36', uid => 5166, passwd => True}
    account::useraccount{'lcgo37': kind => 'studenti', login=>'lcgo37', uid => 5167, passwd => True}
    account::useraccount{'lcgo38': kind => 'studenti', login=>'lcgo38', uid => 5168, passwd => True}
    account::useraccount{'lcgo39': kind => 'studenti', login=>'lcgo39', uid => 5169, passwd => True}
    account::useraccount{'lcgo40': kind => 'studenti', login=>'lcgo40', uid => 5170, passwd => True}
    account::useraccount{'lcgo41': kind => 'studenti', login=>'lcgo41', uid => 5171, passwd => True}
    account::useraccount{'lcgo42': kind => 'studenti', login=>'lcgo42', uid => 5172, passwd => True}
    account::useraccount{'lcgo43': kind => 'studenti', login=>'lcgo43', uid => 5173, passwd => True}
    account::useraccount{'lcgo44': kind => 'studenti', login=>'lcgo44', uid => 5174, passwd => True}
    account::useraccount{'lcgo45': kind => 'studenti', login=>'lcgo45', uid => 5175, passwd => True}
    account::useraccount{'lcgo46': kind => 'studenti', login=>'lcgo46', uid => 5176, passwd => True}
    account::useraccount{'lcgo47': kind => 'studenti', login=>'lcgo47', uid => 5177, passwd => True}
    account::useraccount{'lcgo48': kind => 'studenti', login=>'lcgo48', uid => 5178, passwd => True}
    account::useraccount{'lcgo49': kind => 'studenti', login=>'lcgo49', uid => 5179, passwd => True}
    account::useraccount{'lcgo50': kind => 'studenti', login=>'lcgo50', uid => 5180, passwd => True}
    account::useraccount{'lcgo51': kind => 'studenti', login=>'lcgo51', uid => 6492, passwd => True}
    account::useraccount{'lcgo52': kind => 'studenti', login=>'lcgo52', uid => 6493, passwd => True}
    account::useraccount{'lcgo53': kind => 'studenti', login=>'lcgo53', uid => 6494, passwd => True}
    account::useraccount{'lcgo54': kind => 'studenti', login=>'lcgo54', uid => 6495, passwd => True}
    account::useraccount{'lcgo55': kind => 'studenti', login=>'lcgo55', uid => 6496, passwd => True}
    account::useraccount{'lcgo56': kind => 'studenti', login=>'lcgo56', uid => 6497, passwd => True}
    account::useraccount{'lcgo57': kind => 'studenti', login=>'lcgo57', uid => 6498, passwd => True}
    account::useraccount{'lcgo58': kind => 'studenti', login=>'lcgo58', uid => 6499, passwd => True}
    account::useraccount{'lcgo59': kind => 'studenti', login=>'lcgo59', uid => 6500, passwd => True}
    account::useraccount{'lcgo60': kind => 'studenti', login=>'lcgo60', uid => 6501, passwd => True}
    account::useraccount{'lcgo61': kind => 'studenti', login=>'lcgo61', uid => 5000, passwd => True}

  ## studenti di Giulio D'Agostini
    account::useraccount{'lcgd01': kind => 'studenti', login=>'lcgd01', uid => 5181, passwd => True}
    account::useraccount{'lcgd02': kind => 'studenti', login=>'lcgd02', uid => 5182, passwd => True}
    account::useraccount{'lcgd03': kind => 'studenti', login=>'lcgd03', uid => 5183, passwd => True}
    account::useraccount{'lcgd04': kind => 'studenti', login=>'lcgd04', uid => 5184, passwd => True}
    account::useraccount{'lcgd05': kind => 'studenti', login=>'lcgd05', uid => 5185, passwd => True}
    account::useraccount{'lcgd06': kind => 'studenti', login=>'lcgd06', uid => 5186, passwd => True}
    account::useraccount{'lcgd07': kind => 'studenti', login=>'lcgd07', uid => 5187, passwd => True}
    account::useraccount{'lcgd08': kind => 'studenti', login=>'lcgd08', uid => 5188, passwd => True}
    account::useraccount{'lcgd09': kind => 'studenti', login=>'lcgd09', uid => 5189, passwd => True}
    account::useraccount{'lcgd10': kind => 'studenti', login=>'lcgd10', uid => 5190, passwd => True}
    account::useraccount{'lcgd11': kind => 'studenti', login=>'lcgd11', uid => 5191, passwd => True}
    account::useraccount{'lcgd12': kind => 'studenti', login=>'lcgd12', uid => 5192, passwd => True}
    account::useraccount{'lcgd13': kind => 'studenti', login=>'lcgd13', uid => 5193, passwd => True}
    account::useraccount{'lcgd14': kind => 'studenti', login=>'lcgd14', uid => 5194, passwd => True}
    account::useraccount{'lcgd15': kind => 'studenti', login=>'lcgd15', uid => 5195, passwd => True}
    account::useraccount{'lcgd16': kind => 'studenti', login=>'lcgd16', uid => 5196, passwd => True}
    account::useraccount{'lcgd17': kind => 'studenti', login=>'lcgd17', uid => 5197, passwd => True}
    account::useraccount{'lcgd18': kind => 'studenti', login=>'lcgd18', uid => 5198, passwd => True}
    account::useraccount{'lcgd19': kind => 'studenti', login=>'lcgd19', uid => 5199, passwd => True}
    account::useraccount{'lcgd20': kind => 'studenti', login=>'lcgd20', uid => 5200, passwd => True}
    account::useraccount{'lcgd21': kind => 'studenti', login=>'lcgd21', uid => 5201, passwd => True}
    account::useraccount{'lcgd22': kind => 'studenti', login=>'lcgd22', uid => 5202, passwd => True}
    account::useraccount{'lcgd23': kind => 'studenti', login=>'lcgd23', uid => 5203, passwd => True}
    account::useraccount{'lcgd24': kind => 'studenti', login=>'lcgd24', uid => 5204, passwd => True}
    account::useraccount{'lcgd25': kind => 'studenti', login=>'lcgd25', uid => 5205, passwd => True}
    account::useraccount{'lcgd26': kind => 'studenti', login=>'lcgd26', uid => 5206, passwd => True}
    account::useraccount{'lcgd27': kind => 'studenti', login=>'lcgd27', uid => 5207, passwd => True}
    account::useraccount{'lcgd28': kind => 'studenti', login=>'lcgd28', uid => 5208, passwd => True}
    account::useraccount{'lcgd29': kind => 'studenti', login=>'lcgd29', uid => 5209, passwd => True}
    account::useraccount{'lcgd30': kind => 'studenti', login=>'lcgd30', uid => 5210, passwd => True}
    account::useraccount{'lcgd31': kind => 'studenti', login=>'lcgd31', uid => 5211, passwd => True}
    account::useraccount{'lcgd32': kind => 'studenti', login=>'lcgd32', uid => 5212, passwd => True}
    account::useraccount{'lcgd33': kind => 'studenti', login=>'lcgd33', uid => 5213, passwd => True}
    account::useraccount{'lcgd34': kind => 'studenti', login=>'lcgd34', uid => 5214, passwd => True}
    account::useraccount{'lcgd35': kind => 'studenti', login=>'lcgd35', uid => 5215, passwd => True}
    account::useraccount{'lcgd36': kind => 'studenti', login=>'lcgd36', uid => 5216, passwd => True}
    account::useraccount{'lcgd37': kind => 'studenti', login=>'lcgd37', uid => 5217, passwd => True}
    account::useraccount{'lcgd38': kind => 'studenti', login=>'lcgd38', uid => 5218, passwd => True}
    account::useraccount{'lcgd39': kind => 'studenti', login=>'lcgd39', uid => 5219, passwd => True}
    account::useraccount{'lcgd40': kind => 'studenti', login=>'lcgd40', uid => 5220, passwd => True}
    account::useraccount{'lcgd41': kind => 'studenti', login=>'lcgd41', uid => 5221, passwd => True}
    account::useraccount{'lcgd42': kind => 'studenti', login=>'lcgd42', uid => 5222, passwd => True}
    account::useraccount{'lcgd43': kind => 'studenti', login=>'lcgd43', uid => 5223, passwd => True}
    account::useraccount{'lcgd44': kind => 'studenti', login=>'lcgd44', uid => 5224, passwd => True}
    account::useraccount{'lcgd45': kind => 'studenti', login=>'lcgd45', uid => 5225, passwd => True}
    account::useraccount{'lcgd46': kind => 'studenti', login=>'lcgd46', uid => 5226, passwd => True}
    account::useraccount{'lcgd47': kind => 'studenti', login=>'lcgd47', uid => 5227, passwd => True}
    account::useraccount{'lcgd48': kind => 'studenti', login=>'lcgd48', uid => 5228, passwd => True}
    account::useraccount{'lcgd49': kind => 'studenti', login=>'lcgd49', uid => 5229, passwd => True}
    account::useraccount{'lcgd50': kind => 'studenti', login=>'lcgd50', uid => 5230, passwd => True}
    account::useraccount{'lcgd51': kind => 'studenti', login=>'lcgd51', uid => 6502, passwd => True}
    account::useraccount{'lcgd52': kind => 'studenti', login=>'lcgd52', uid => 6503, passwd => True}
    account::useraccount{'lcgd53': kind => 'studenti', login=>'lcgd53', uid => 6504, passwd => True}
    account::useraccount{'lcgd54': kind => 'studenti', login=>'lcgd54', uid => 6505, passwd => True}
    account::useraccount{'lcgd55': kind => 'studenti', login=>'lcgd55', uid => 6506, passwd => True}
    account::useraccount{'lcgd56': kind => 'studenti', login=>'lcgd56', uid => 6507, passwd => True}
    account::useraccount{'lcgd57': kind => 'studenti', login=>'lcgd57', uid => 6508, passwd => True}
    account::useraccount{'lcgd58': kind => 'studenti', login=>'lcgd58', uid => 6509, passwd => True}
    account::useraccount{'lcgd59': kind => 'studenti', login=>'lcgd59', uid => 6510, passwd => True}
    account::useraccount{'lcgd60': kind => 'studenti', login=>'lcgd60', uid => 6511, passwd => True}

  # Laboratorio di Fisica Computazionale 1 (secondo anno - Maiorano/Marinari/Pani/Ricci-Tersenghi/Angelini/Leuzzi/DeMichele/Viale)
# Per disabilitare temporaneamente gli account (es. durante esami)
# cambiare il valore di defshell a '/sbin/nologin' e impostare i permessi di default a '700'
# Esempio: disabilitato
#    account::useraccount{'labfc000': kind => 'studenti', login=>'labfc000', uid => 6231, passwd => True}
# Esempio: abilitato
#    account::useraccount{'labfc000': kind => 'studenti', login=>'labfc000', uid => 6231, passwd => True}
  
    account::useraccount{'labfc000': kind => 'studenti', login=>'labfc000', uid => 6231, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc001': kind => 'studenti', login=>'labfc001', uid => 6232, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc002': kind => 'studenti', login=>'labfc002', uid => 6233, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc003': kind => 'studenti', login=>'labfc003', uid => 6234, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc004': kind => 'studenti', login=>'labfc004', uid => 6235, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc005': kind => 'studenti', login=>'labfc005', uid => 6236, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc006': kind => 'studenti', login=>'labfc006', uid => 6237, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc007': kind => 'studenti', login=>'labfc007', uid => 6238, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc008': kind => 'studenti', login=>'labfc008', uid => 6239, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc009': kind => 'studenti', login=>'labfc009', uid => 6240, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc010': kind => 'studenti', login=>'labfc010', uid => 6241, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc011': kind => 'studenti', login=>'labfc011', uid => 6242, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc012': kind => 'studenti', login=>'labfc012', uid => 6243, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc013': kind => 'studenti', login=>'labfc013', uid => 6244, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc014': kind => 'studenti', login=>'labfc014', uid => 6245, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc015': kind => 'studenti', login=>'labfc015', uid => 6246, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc016': kind => 'studenti', login=>'labfc016', uid => 6247, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc017': kind => 'studenti', login=>'labfc017', uid => 6248, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc018': kind => 'studenti', login=>'labfc018', uid => 6249, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc019': kind => 'studenti', login=>'labfc019', uid => 6250, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc020': kind => 'studenti', login=>'labfc020', uid => 6251, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc021': kind => 'studenti', login=>'labfc021', uid => 6252, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc022': kind => 'studenti', login=>'labfc022', uid => 6253, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc023': kind => 'studenti', login=>'labfc023', uid => 6254, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc024': kind => 'studenti', login=>'labfc024', uid => 6255, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc025': kind => 'studenti', login=>'labfc025', uid => 6256, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc026': kind => 'studenti', login=>'labfc026', uid => 6257, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc027': kind => 'studenti', login=>'labfc027', uid => 6258, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc028': kind => 'studenti', login=>'labfc028', uid => 6259, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc029': kind => 'studenti', login=>'labfc029', uid => 6260, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc030': kind => 'studenti', login=>'labfc030', uid => 6261, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc031': kind => 'studenti', login=>'labfc031', uid => 6262, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc032': kind => 'studenti', login=>'labfc032', uid => 6263, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc033': kind => 'studenti', login=>'labfc033', uid => 6264, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc034': kind => 'studenti', login=>'labfc034', uid => 6265, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc035': kind => 'studenti', login=>'labfc035', uid => 6266, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc036': kind => 'studenti', login=>'labfc036', uid => 6267, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc037': kind => 'studenti', login=>'labfc037', uid => 6268, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc038': kind => 'studenti', login=>'labfc038', uid => 6269, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc039': kind => 'studenti', login=>'labfc039', uid => 6270, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc040': kind => 'studenti', login=>'labfc040', uid => 6271, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc041': kind => 'studenti', login=>'labfc041', uid => 6272, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc042': kind => 'studenti', login=>'labfc042', uid => 6273, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc043': kind => 'studenti', login=>'labfc043', uid => 6274, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc044': kind => 'studenti', login=>'labfc044', uid => 6275, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc045': kind => 'studenti', login=>'labfc045', uid => 6276, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc046': kind => 'studenti', login=>'labfc046', uid => 6277, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc047': kind => 'studenti', login=>'labfc047', uid => 6278, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc048': kind => 'studenti', login=>'labfc048', uid => 6279, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc049': kind => 'studenti', login=>'labfc049', uid => 6280, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc050': kind => 'studenti', login=>'labfc050', uid => 6281, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc100': kind => 'studenti', login=>'labfc100', uid => 6331, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc101': kind => 'studenti', login=>'labfc101', uid => 6332, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc102': kind => 'studenti', login=>'labfc102', uid => 6333, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc103': kind => 'studenti', login=>'labfc103', uid => 6334, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc104': kind => 'studenti', login=>'labfc104', uid => 6335, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc105': kind => 'studenti', login=>'labfc105', uid => 6336, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc106': kind => 'studenti', login=>'labfc106', uid => 6337, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc107': kind => 'studenti', login=>'labfc107', uid => 6338, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc108': kind => 'studenti', login=>'labfc108', uid => 6339, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc109': kind => 'studenti', login=>'labfc109', uid => 6340, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc110': kind => 'studenti', login=>'labfc110', uid => 6341, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc111': kind => 'studenti', login=>'labfc111', uid => 6342, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc112': kind => 'studenti', login=>'labfc112', uid => 6343, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc113': kind => 'studenti', login=>'labfc113', uid => 6344, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc114': kind => 'studenti', login=>'labfc114', uid => 6345, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc115': kind => 'studenti', login=>'labfc115', uid => 6346, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc116': kind => 'studenti', login=>'labfc116', uid => 6347, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc117': kind => 'studenti', login=>'labfc117', uid => 6348, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc118': kind => 'studenti', login=>'labfc118', uid => 6349, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc119': kind => 'studenti', login=>'labfc119', uid => 6350, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc120': kind => 'studenti', login=>'labfc120', uid => 6351, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc121': kind => 'studenti', login=>'labfc121', uid => 6352, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc122': kind => 'studenti', login=>'labfc122', uid => 6353, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc123': kind => 'studenti', login=>'labfc123', uid => 6354, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc124': kind => 'studenti', login=>'labfc124', uid => 6355, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc125': kind => 'studenti', login=>'labfc125', uid => 6356, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc126': kind => 'studenti', login=>'labfc126', uid => 6357, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc127': kind => 'studenti', login=>'labfc127', uid => 6358, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc128': kind => 'studenti', login=>'labfc128', uid => 6359, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc129': kind => 'studenti', login=>'labfc129', uid => 6360, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc130': kind => 'studenti', login=>'labfc130', uid => 6361, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc131': kind => 'studenti', login=>'labfc131', uid => 6362, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc132': kind => 'studenti', login=>'labfc132', uid => 6363, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc133': kind => 'studenti', login=>'labfc133', uid => 6364, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc134': kind => 'studenti', login=>'labfc134', uid => 6365, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc135': kind => 'studenti', login=>'labfc135', uid => 6366, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc136': kind => 'studenti', login=>'labfc136', uid => 6367, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc137': kind => 'studenti', login=>'labfc137', uid => 6368, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc138': kind => 'studenti', login=>'labfc138', uid => 6369, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc139': kind => 'studenti', login=>'labfc139', uid => 6370, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc140': kind => 'studenti', login=>'labfc140', uid => 6371, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc141': kind => 'studenti', login=>'labfc141', uid => 6372, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc142': kind => 'studenti', login=>'labfc142', uid => 6373, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc143': kind => 'studenti', login=>'labfc143', uid => 6374, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc144': kind => 'studenti', login=>'labfc144', uid => 6375, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc145': kind => 'studenti', login=>'labfc145', uid => 6376, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc146': kind => 'studenti', login=>'labfc146', uid => 6377, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc147': kind => 'studenti', login=>'labfc147', uid => 6378, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc148': kind => 'studenti', login=>'labfc148', uid => 6379, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc149': kind => 'studenti', login=>'labfc149', uid => 6380, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc150': kind => 'studenti', login=>'labfc150', uid => 6381, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc200': kind => 'studenti', login=>'labfc200', uid => 6431, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc201': kind => 'studenti', login=>'labfc201', uid => 6432, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc202': kind => 'studenti', login=>'labfc202', uid => 6433, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc203': kind => 'studenti', login=>'labfc203', uid => 6434, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc204': kind => 'studenti', login=>'labfc204', uid => 6435, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc205': kind => 'studenti', login=>'labfc205', uid => 6436, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc206': kind => 'studenti', login=>'labfc206', uid => 6437, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc207': kind => 'studenti', login=>'labfc207', uid => 6438, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc208': kind => 'studenti', login=>'labfc208', uid => 6439, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc209': kind => 'studenti', login=>'labfc209', uid => 6440, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc210': kind => 'studenti', login=>'labfc210', uid => 6441, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc211': kind => 'studenti', login=>'labfc211', uid => 6442, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc212': kind => 'studenti', login=>'labfc212', uid => 6443, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc213': kind => 'studenti', login=>'labfc213', uid => 6444, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc214': kind => 'studenti', login=>'labfc214', uid => 6445, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc215': kind => 'studenti', login=>'labfc215', uid => 6446, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc216': kind => 'studenti', login=>'labfc216', uid => 6447, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc217': kind => 'studenti', login=>'labfc217', uid => 6448, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc218': kind => 'studenti', login=>'labfc218', uid => 6449, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc219': kind => 'studenti', login=>'labfc219', uid => 6450, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc220': kind => 'studenti', login=>'labfc220', uid => 6451, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc221': kind => 'studenti', login=>'labfc221', uid => 6452, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc222': kind => 'studenti', login=>'labfc222', uid => 6453, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc223': kind => 'studenti', login=>'labfc223', uid => 6454, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc224': kind => 'studenti', login=>'labfc224', uid => 6455, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc225': kind => 'studenti', login=>'labfc225', uid => 6456, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc226': kind => 'studenti', login=>'labfc226', uid => 6457, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc227': kind => 'studenti', login=>'labfc227', uid => 6458, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc228': kind => 'studenti', login=>'labfc228', uid => 6459, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc229': kind => 'studenti', login=>'labfc229', uid => 6460, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc230': kind => 'studenti', login=>'labfc230', uid => 6461, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc231': kind => 'studenti', login=>'labfc231', uid => 6462, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc232': kind => 'studenti', login=>'labfc232', uid => 6463, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc233': kind => 'studenti', login=>'labfc233', uid => 6464, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc234': kind => 'studenti', login=>'labfc234', uid => 6465, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc235': kind => 'studenti', login=>'labfc235', uid => 6466, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc236': kind => 'studenti', login=>'labfc236', uid => 6467, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc237': kind => 'studenti', login=>'labfc237', uid => 6468, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc238': kind => 'studenti', login=>'labfc238', uid => 6469, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc239': kind => 'studenti', login=>'labfc239', uid => 6470, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc240': kind => 'studenti', login=>'labfc240', uid => 6471, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc241': kind => 'studenti', login=>'labfc241', uid => 6472, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc242': kind => 'studenti', login=>'labfc242', uid => 6473, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc243': kind => 'studenti', login=>'labfc243', uid => 6474, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc244': kind => 'studenti', login=>'labfc244', uid => 6475, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc245': kind => 'studenti', login=>'labfc245', uid => 6476, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc246': kind => 'studenti', login=>'labfc246', uid => 6477, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc247': kind => 'studenti', login=>'labfc247', uid => 6478, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc248': kind => 'studenti', login=>'labfc248', uid => 6479, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc249': kind => 'studenti', login=>'labfc249', uid => 6480, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc250': kind => 'studenti', login=>'labfc250', uid => 6481, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc300': kind => 'studenti', login=>'labfc300', uid => 6531, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc301': kind => 'studenti', login=>'labfc301', uid => 6532, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc302': kind => 'studenti', login=>'labfc302', uid => 6533, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc303': kind => 'studenti', login=>'labfc303', uid => 6534, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc304': kind => 'studenti', login=>'labfc304', uid => 6535, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc305': kind => 'studenti', login=>'labfc305', uid => 6536, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc306': kind => 'studenti', login=>'labfc306', uid => 6537, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc307': kind => 'studenti', login=>'labfc307', uid => 6538, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc308': kind => 'studenti', login=>'labfc308', uid => 6539, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc309': kind => 'studenti', login=>'labfc309', uid => 6540, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc310': kind => 'studenti', login=>'labfc310', uid => 6541, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc311': kind => 'studenti', login=>'labfc311', uid => 6542, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc312': kind => 'studenti', login=>'labfc312', uid => 6543, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc313': kind => 'studenti', login=>'labfc313', uid => 6544, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc314': kind => 'studenti', login=>'labfc314', uid => 6545, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc315': kind => 'studenti', login=>'labfc315', uid => 6546, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc316': kind => 'studenti', login=>'labfc316', uid => 6547, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc317': kind => 'studenti', login=>'labfc317', uid => 6548, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc318': kind => 'studenti', login=>'labfc318', uid => 6549, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc319': kind => 'studenti', login=>'labfc319', uid => 6550, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc320': kind => 'studenti', login=>'labfc320', uid => 6551, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc321': kind => 'studenti', login=>'labfc321', uid => 6552, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc322': kind => 'studenti', login=>'labfc322', uid => 6553, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc323': kind => 'studenti', login=>'labfc323', uid => 6554, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc324': kind => 'studenti', login=>'labfc324', uid => 6555, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc325': kind => 'studenti', login=>'labfc325', uid => 6556, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc326': kind => 'studenti', login=>'labfc326', uid => 6557, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc327': kind => 'studenti', login=>'labfc327', uid => 6558, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc328': kind => 'studenti', login=>'labfc328', uid => 6559, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc329': kind => 'studenti', login=>'labfc329', uid => 6560, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc330': kind => 'studenti', login=>'labfc330', uid => 6561, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc331': kind => 'studenti', login=>'labfc331', uid => 6562, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc332': kind => 'studenti', login=>'labfc332', uid => 6563, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc333': kind => 'studenti', login=>'labfc333', uid => 6564, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc334': kind => 'studenti', login=>'labfc334', uid => 6565, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc335': kind => 'studenti', login=>'labfc335', uid => 6566, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc336': kind => 'studenti', login=>'labfc336', uid => 6567, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc337': kind => 'studenti', login=>'labfc337', uid => 6568, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc338': kind => 'studenti', login=>'labfc338', uid => 6569, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc339': kind => 'studenti', login=>'labfc339', uid => 6570, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc340': kind => 'studenti', login=>'labfc340', uid => 6571, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc341': kind => 'studenti', login=>'labfc341', uid => 6572, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc342': kind => 'studenti', login=>'labfc342', uid => 6573, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc343': kind => 'studenti', login=>'labfc343', uid => 6574, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc344': kind => 'studenti', login=>'labfc344', uid => 6575, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc345': kind => 'studenti', login=>'labfc345', uid => 6576, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc346': kind => 'studenti', login=>'labfc346', uid => 6577, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc347': kind => 'studenti', login=>'labfc347', uid => 6578, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc348': kind => 'studenti', login=>'labfc348', uid => 6579, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc349': kind => 'studenti', login=>'labfc349', uid => 6580, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc350': kind => 'studenti', login=>'labfc350', uid => 6581, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc400': kind => 'studenti', login=>'labfc400', uid => 6631, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc401': kind => 'studenti', login=>'labfc401', uid => 6632, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc402': kind => 'studenti', login=>'labfc402', uid => 6633, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc403': kind => 'studenti', login=>'labfc403', uid => 6634, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc404': kind => 'studenti', login=>'labfc404', uid => 6635, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc405': kind => 'studenti', login=>'labfc405', uid => 6636, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc406': kind => 'studenti', login=>'labfc406', uid => 6637, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc407': kind => 'studenti', login=>'labfc407', uid => 6638, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc408': kind => 'studenti', login=>'labfc408', uid => 6639, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc409': kind => 'studenti', login=>'labfc409', uid => 6640, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc410': kind => 'studenti', login=>'labfc410', uid => 6641, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc411': kind => 'studenti', login=>'labfc411', uid => 6642, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc412': kind => 'studenti', login=>'labfc412', uid => 6643, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc413': kind => 'studenti', login=>'labfc413', uid => 6644, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc414': kind => 'studenti', login=>'labfc414', uid => 6645, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc415': kind => 'studenti', login=>'labfc415', uid => 6646, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc416': kind => 'studenti', login=>'labfc416', uid => 6647, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc417': kind => 'studenti', login=>'labfc417', uid => 6648, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc418': kind => 'studenti', login=>'labfc418', uid => 6649, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc419': kind => 'studenti', login=>'labfc419', uid => 6650, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc420': kind => 'studenti', login=>'labfc420', uid => 6651, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc421': kind => 'studenti', login=>'labfc421', uid => 6652, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc422': kind => 'studenti', login=>'labfc422', uid => 6653, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc423': kind => 'studenti', login=>'labfc423', uid => 6654, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc424': kind => 'studenti', login=>'labfc424', uid => 6655, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc425': kind => 'studenti', login=>'labfc425', uid => 6656, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc426': kind => 'studenti', login=>'labfc426', uid => 6657, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc427': kind => 'studenti', login=>'labfc427', uid => 6658, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc428': kind => 'studenti', login=>'labfc428', uid => 6659, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc429': kind => 'studenti', login=>'labfc429', uid => 6660, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc430': kind => 'studenti', login=>'labfc430', uid => 6661, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc431': kind => 'studenti', login=>'labfc431', uid => 6662, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc432': kind => 'studenti', login=>'labfc432', uid => 6663, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc433': kind => 'studenti', login=>'labfc433', uid => 6664, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc434': kind => 'studenti', login=>'labfc434', uid => 6665, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc435': kind => 'studenti', login=>'labfc435', uid => 6666, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc436': kind => 'studenti', login=>'labfc436', uid => 6667, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc437': kind => 'studenti', login=>'labfc437', uid => 6668, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc438': kind => 'studenti', login=>'labfc438', uid => 6669, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc439': kind => 'studenti', login=>'labfc439', uid => 6670, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc440': kind => 'studenti', login=>'labfc440', uid => 6671, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc441': kind => 'studenti', login=>'labfc441', uid => 6672, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc442': kind => 'studenti', login=>'labfc442', uid => 6673, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc443': kind => 'studenti', login=>'labfc443', uid => 6674, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc444': kind => 'studenti', login=>'labfc444', uid => 6675, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc445': kind => 'studenti', login=>'labfc445', uid => 6676, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc446': kind => 'studenti', login=>'labfc446', uid => 6677, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc447': kind => 'studenti', login=>'labfc447', uid => 6678, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc448': kind => 'studenti', login=>'labfc448', uid => 6679, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc449': kind => 'studenti', login=>'labfc449', uid => 6680, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc450': kind => 'studenti', login=>'labfc450', uid => 6681, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc500': kind => 'studenti', login=>'labfc500', uid => 6731, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc501': kind => 'studenti', login=>'labfc501', uid => 6732, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc502': kind => 'studenti', login=>'labfc502', uid => 6733, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc503': kind => 'studenti', login=>'labfc503', uid => 6734, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc504': kind => 'studenti', login=>'labfc504', uid => 6735, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc505': kind => 'studenti', login=>'labfc505', uid => 6736, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc506': kind => 'studenti', login=>'labfc506', uid => 6737, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc507': kind => 'studenti', login=>'labfc507', uid => 6738, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc508': kind => 'studenti', login=>'labfc508', uid => 6739, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc509': kind => 'studenti', login=>'labfc509', uid => 6740, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc510': kind => 'studenti', login=>'labfc510', uid => 6741, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc511': kind => 'studenti', login=>'labfc511', uid => 6742, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc512': kind => 'studenti', login=>'labfc512', uid => 6743, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc513': kind => 'studenti', login=>'labfc513', uid => 6744, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc514': kind => 'studenti', login=>'labfc514', uid => 6745, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc515': kind => 'studenti', login=>'labfc515', uid => 6746, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc516': kind => 'studenti', login=>'labfc516', uid => 6747, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc517': kind => 'studenti', login=>'labfc517', uid => 6748, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc518': kind => 'studenti', login=>'labfc518', uid => 6749, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc519': kind => 'studenti', login=>'labfc519', uid => 6750, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc520': kind => 'studenti', login=>'labfc520', uid => 6751, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc521': kind => 'studenti', login=>'labfc521', uid => 6752, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc522': kind => 'studenti', login=>'labfc522', uid => 6753, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc523': kind => 'studenti', login=>'labfc523', uid => 6754, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc524': kind => 'studenti', login=>'labfc524', uid => 6755, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc525': kind => 'studenti', login=>'labfc525', uid => 6756, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc526': kind => 'studenti', login=>'labfc526', uid => 6757, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc527': kind => 'studenti', login=>'labfc527', uid => 6758, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc528': kind => 'studenti', login=>'labfc528', uid => 6759, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc529': kind => 'studenti', login=>'labfc529', uid => 6760, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc530': kind => 'studenti', login=>'labfc530', uid => 6761, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc531': kind => 'studenti', login=>'labfc531', uid => 6762, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc532': kind => 'studenti', login=>'labfc532', uid => 6763, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc533': kind => 'studenti', login=>'labfc533', uid => 6764, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc534': kind => 'studenti', login=>'labfc534', uid => 6765, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc535': kind => 'studenti', login=>'labfc535', uid => 6766, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc536': kind => 'studenti', login=>'labfc536', uid => 6767, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc537': kind => 'studenti', login=>'labfc537', uid => 6768, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc538': kind => 'studenti', login=>'labfc538', uid => 6769, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc539': kind => 'studenti', login=>'labfc539', uid => 6770, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc540': kind => 'studenti', login=>'labfc540', uid => 6771, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc541': kind => 'studenti', login=>'labfc541', uid => 6772, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc542': kind => 'studenti', login=>'labfc542', uid => 6773, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc543': kind => 'studenti', login=>'labfc543', uid => 6774, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc544': kind => 'studenti', login=>'labfc544', uid => 6775, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc545': kind => 'studenti', login=>'labfc545', uid => 6776, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc546': kind => 'studenti', login=>'labfc546', uid => 6777, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc547': kind => 'studenti', login=>'labfc547', uid => 6778, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc548': kind => 'studenti', login=>'labfc548', uid => 6779, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc549': kind => 'studenti', login=>'labfc549', uid => 6780, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }
    account::useraccount{'labfc550': kind => 'studenti', login=>'labfc550', uid => 6781, passwd => True, defperms => '700' , defshell => '/sbin/nologin' }

  #Esami lfc1 Leuzzi/Angelini/Pani 2018
#Esempio account abilitato
    account::useraccount{'E1747101': kind => 'studenti', login=>'E1747101', uid => 9201, passwd => True}
# Per disabilitare, cambiare la shell /bin/bash con /sbin/nologin.
# Gli utenti del gruppo 'esami' hanno permessi 700 di default.  
  account::useraccount{'F101': kind => 'esami', login=>'F101', uid => 10101, passwd => True}
  account::useraccount{'F102': kind => 'esami', login=>'F102', uid => 10102, passwd => True}
  account::useraccount{'F103': kind => 'esami', login=>'F103', uid => 10103, passwd => True}
  account::useraccount{'F104': kind => 'esami', login=>'F104', uid => 10104, passwd => True}
  account::useraccount{'F105': kind => 'esami', login=>'F105', uid => 10105, passwd => True}
  account::useraccount{'F106': kind => 'esami', login=>'F106', uid => 10106, passwd => True}
  account::useraccount{'F107': kind => 'esami', login=>'F107', uid => 10107, passwd => True}
  account::useraccount{'F108': kind => 'esami', login=>'F108', uid => 10108, passwd => True}
  account::useraccount{'F109': kind => 'esami', login=>'F109', uid => 10109, passwd => True}
  account::useraccount{'F110': kind => 'esami', login=>'F110', uid => 10110, passwd => True}
  account::useraccount{'F111': kind => 'esami', login=>'F111', uid => 10111, passwd => True}
  account::useraccount{'F112': kind => 'esami', login=>'F112', uid => 10112, passwd => True}
  account::useraccount{'F113': kind => 'esami', login=>'F113', uid => 10113, passwd => True}
  account::useraccount{'F114': kind => 'esami', login=>'F114', uid => 10114, passwd => True}
  account::useraccount{'F115': kind => 'esami', login=>'F115', uid => 10115, passwd => True}
  account::useraccount{'F116': kind => 'esami', login=>'F116', uid => 10116, passwd => True}
  account::useraccount{'F117': kind => 'esami', login=>'F117', uid => 10117, passwd => True}
  account::useraccount{'F118': kind => 'esami', login=>'F118', uid => 10118, passwd => True}
  account::useraccount{'F119': kind => 'esami', login=>'F119', uid => 10119, passwd => True}
  account::useraccount{'F120': kind => 'esami', login=>'F120', uid => 10120, passwd => True}
  account::useraccount{'F121': kind => 'esami', login=>'F121', uid => 10121, passwd => True}
  account::useraccount{'F122': kind => 'esami', login=>'F122', uid => 10122, passwd => True}
  account::useraccount{'F123': kind => 'esami', login=>'F123', uid => 10123, passwd => True}
  account::useraccount{'F124': kind => 'esami', login=>'F124', uid => 10124, passwd => True}
  account::useraccount{'F125': kind => 'esami', login=>'F125', uid => 10125, passwd => True}
  account::useraccount{'F126': kind => 'esami', login=>'F126', uid => 10126, passwd => True}
  account::useraccount{'F127': kind => 'esami', login=>'F127', uid => 10127, passwd => True}
  account::useraccount{'F128': kind => 'esami', login=>'F128', uid => 10128, passwd => True}
  account::useraccount{'F129': kind => 'esami', login=>'F129', uid => 10129, passwd => True}
  account::useraccount{'F130': kind => 'esami', login=>'F130', uid => 10130, passwd => True}
  account::useraccount{'F131': kind => 'esami', login=>'F131', uid => 10131, passwd => True}
  account::useraccount{'F132': kind => 'esami', login=>'F132', uid => 10132, passwd => True}
  account::useraccount{'F133': kind => 'esami', login=>'F133', uid => 10133, passwd => True}
  account::useraccount{'F134': kind => 'esami', login=>'F134', uid => 10134, passwd => True}
  account::useraccount{'F135': kind => 'esami', login=>'F135', uid => 10135, passwd => True}
  account::useraccount{'F136': kind => 'esami', login=>'F136', uid => 10136, passwd => True}
  account::useraccount{'F137': kind => 'esami', login=>'F137', uid => 10137, passwd => True}
  account::useraccount{'F138': kind => 'esami', login=>'F138', uid => 10138, passwd => True}
  account::useraccount{'F139': kind => 'esami', login=>'F139', uid => 10139, passwd => True}
  account::useraccount{'F140': kind => 'esami', login=>'F140', uid => 10140, passwd => True}

  account::useraccount{'F201': kind => 'esami', login=>'F201', uid => 10201, passwd => True}
  account::useraccount{'F202': kind => 'esami', login=>'F202', uid => 10202, passwd => True}
  account::useraccount{'F203': kind => 'esami', login=>'F203', uid => 10203, passwd => True}
  account::useraccount{'F204': kind => 'esami', login=>'F204', uid => 10204, passwd => True}
  account::useraccount{'F205': kind => 'esami', login=>'F205', uid => 10205, passwd => True}
  account::useraccount{'F206': kind => 'esami', login=>'F206', uid => 10206, passwd => True}
  account::useraccount{'F207': kind => 'esami', login=>'F207', uid => 10207, passwd => True}
  account::useraccount{'F208': kind => 'esami', login=>'F208', uid => 10208, passwd => True}
  account::useraccount{'F209': kind => 'esami', login=>'F209', uid => 10209, passwd => True}
  account::useraccount{'F210': kind => 'esami', login=>'F210', uid => 10210, passwd => True}
  account::useraccount{'F211': kind => 'esami', login=>'F211', uid => 10211, passwd => True}
  account::useraccount{'F212': kind => 'esami', login=>'F212', uid => 10212, passwd => True}
  account::useraccount{'F213': kind => 'esami', login=>'F213', uid => 10213, passwd => True}
  account::useraccount{'F214': kind => 'esami', login=>'F214', uid => 10214, passwd => True}
  account::useraccount{'F215': kind => 'esami', login=>'F215', uid => 10215, passwd => True}
  account::useraccount{'F216': kind => 'esami', login=>'F216', uid => 10216, passwd => True}
  account::useraccount{'F217': kind => 'esami', login=>'F217', uid => 10217, passwd => True}
  account::useraccount{'F218': kind => 'esami', login=>'F218', uid => 10218, passwd => True}
  account::useraccount{'F219': kind => 'esami', login=>'F219', uid => 10219, passwd => True}
  account::useraccount{'F220': kind => 'esami', login=>'F220', uid => 10220, passwd => True}
  account::useraccount{'F221': kind => 'esami', login=>'F221', uid => 10221, passwd => True}
  account::useraccount{'F222': kind => 'esami', login=>'F222', uid => 10222, passwd => True}
  account::useraccount{'F223': kind => 'esami', login=>'F223', uid => 10223, passwd => True}
  account::useraccount{'F224': kind => 'esami', login=>'F224', uid => 10224, passwd => True}
  account::useraccount{'F225': kind => 'esami', login=>'F225', uid => 10225, passwd => True}
  account::useraccount{'F226': kind => 'esami', login=>'F226', uid => 10226, passwd => True}
  account::useraccount{'F227': kind => 'esami', login=>'F227', uid => 10227, passwd => True}
  account::useraccount{'F228': kind => 'esami', login=>'F228', uid => 10228, passwd => True}
  account::useraccount{'F229': kind => 'esami', login=>'F229', uid => 10229, passwd => True}
  account::useraccount{'F230': kind => 'esami', login=>'F230', uid => 10230, passwd => True}
  account::useraccount{'F231': kind => 'esami', login=>'F231', uid => 10231, passwd => True}
  account::useraccount{'F232': kind => 'esami', login=>'F232', uid => 10232, passwd => True}
  account::useraccount{'F233': kind => 'esami', login=>'F233', uid => 10233, passwd => True}
  account::useraccount{'F234': kind => 'esami', login=>'F234', uid => 10234, passwd => True}
  account::useraccount{'F235': kind => 'esami', login=>'F235', uid => 10235, passwd => True}
  account::useraccount{'F236': kind => 'esami', login=>'F236', uid => 10236, passwd => True}
  account::useraccount{'F237': kind => 'esami', login=>'F237', uid => 10237, passwd => True}
  account::useraccount{'F238': kind => 'esami', login=>'F238', uid => 10238, passwd => True}
  account::useraccount{'F239': kind => 'esami', login=>'F239', uid => 10239, passwd => True}
  account::useraccount{'F240': kind => 'esami', login=>'F240', uid => 10240, passwd => True}

  account::useraccount{'F301': kind => 'esami', login=>'F301', uid => 10301, passwd => True}
  account::useraccount{'F302': kind => 'esami', login=>'F302', uid => 10302, passwd => True}
  account::useraccount{'F303': kind => 'esami', login=>'F303', uid => 10303, passwd => True}
  account::useraccount{'F304': kind => 'esami', login=>'F304', uid => 10304, passwd => True}
  account::useraccount{'F305': kind => 'esami', login=>'F305', uid => 10305, passwd => True}
  account::useraccount{'F306': kind => 'esami', login=>'F306', uid => 10306, passwd => True}
  account::useraccount{'F307': kind => 'esami', login=>'F307', uid => 10307, passwd => True}
  account::useraccount{'F308': kind => 'esami', login=>'F308', uid => 10308, passwd => True}
  account::useraccount{'F309': kind => 'esami', login=>'F309', uid => 10309, passwd => True}
  account::useraccount{'F310': kind => 'esami', login=>'F310', uid => 10310, passwd => True}
  account::useraccount{'F311': kind => 'esami', login=>'F311', uid => 10311, passwd => True}
  account::useraccount{'F312': kind => 'esami', login=>'F312', uid => 10312, passwd => True}
  account::useraccount{'F313': kind => 'esami', login=>'F313', uid => 10313, passwd => True}
  account::useraccount{'F314': kind => 'esami', login=>'F314', uid => 10314, passwd => True}
  account::useraccount{'F315': kind => 'esami', login=>'F315', uid => 10315, passwd => True}
  account::useraccount{'F316': kind => 'esami', login=>'F316', uid => 10316, passwd => True}
  account::useraccount{'F317': kind => 'esami', login=>'F317', uid => 10317, passwd => True}
  account::useraccount{'F318': kind => 'esami', login=>'F318', uid => 10318, passwd => True}
  account::useraccount{'F319': kind => 'esami', login=>'F319', uid => 10319, passwd => True}
  account::useraccount{'F320': kind => 'esami', login=>'F320', uid => 10320, passwd => True}
  account::useraccount{'F321': kind => 'esami', login=>'F321', uid => 10321, passwd => True}
  account::useraccount{'F322': kind => 'esami', login=>'F322', uid => 10322, passwd => True}
  account::useraccount{'F323': kind => 'esami', login=>'F323', uid => 10323, passwd => True}
  account::useraccount{'F324': kind => 'esami', login=>'F324', uid => 10324, passwd => True}
  account::useraccount{'F325': kind => 'esami', login=>'F325', uid => 10325, passwd => True}
  account::useraccount{'F326': kind => 'esami', login=>'F326', uid => 10326, passwd => True}
  account::useraccount{'F327': kind => 'esami', login=>'F327', uid => 10327, passwd => True}
  account::useraccount{'F328': kind => 'esami', login=>'F328', uid => 10328, passwd => True}
  account::useraccount{'F329': kind => 'esami', login=>'F329', uid => 10329, passwd => True}
  account::useraccount{'F330': kind => 'esami', login=>'F330', uid => 10330, passwd => True}
  account::useraccount{'F331': kind => 'esami', login=>'F331', uid => 10331, passwd => True}
  account::useraccount{'F332': kind => 'esami', login=>'F332', uid => 10332, passwd => True}
  account::useraccount{'F333': kind => 'esami', login=>'F333', uid => 10333, passwd => True}
  account::useraccount{'F334': kind => 'esami', login=>'F334', uid => 10334, passwd => True}
  account::useraccount{'F335': kind => 'esami', login=>'F335', uid => 10335, passwd => True}
  account::useraccount{'F336': kind => 'esami', login=>'F336', uid => 10336, passwd => True}
  account::useraccount{'F337': kind => 'esami', login=>'F337', uid => 10337, passwd => True}
  account::useraccount{'F338': kind => 'esami', login=>'F338', uid => 10338, passwd => True}
  account::useraccount{'F339': kind => 'esami', login=>'F339', uid => 10339, passwd => True}
  account::useraccount{'F340': kind => 'esami', login=>'F340', uid => 10340, passwd => True}
  
  #TURNO A
    account::useraccount{'F1796253': kind => 'studenti', login=>'F1796253', uid => 8101, passwd => True}
    account::useraccount{'F1755772': kind => 'studenti', login=>'F1755772', uid => 8027, passwd => True}
    account::useraccount{'F1811314': kind => 'studenti', login=>'F1811314', uid => 8205, passwd => True}
    account::useraccount{'F1792595': kind => 'studenti', login=>'F1792595', uid => 8061, passwd => True}
    account::useraccount{'F1813352': kind => 'studenti', login=>'F1813352', uid => 8215, passwd => True}
    account::useraccount{'F1785169': kind => 'studenti', login=>'F1785169', uid => 8048, passwd => True}
    account::useraccount{'F1653592': kind => 'studenti', login=>'F1653592', uid => 8012, passwd => True}
    account::useraccount{'F1809668': kind => 'studenti', login=>'F1809668', uid => 8193, passwd => True}
    account::useraccount{'F1799302': kind => 'studenti', login=>'F1799302', uid => 8129, passwd => True}
    account::useraccount{'F1816400': kind => 'studenti', login=>'F1816400', uid => 8242, passwd => True}
    account::useraccount{'F1804163': kind => 'studenti', login=>'F1804163', uid => 8269, passwd => True}
    account::useraccount{'F1813494': kind => 'studenti', login=>'F1813494', uid => 8217, passwd => True}
    account::useraccount{'F1790024': kind => 'studenti', login=>'F1790024', uid => 8057, passwd => True}
    account::useraccount{'F1802650': kind => 'studenti', login=>'F1802650', uid => 8148, passwd => True}
    account::useraccount{'F1635678': kind => 'studenti', login=>'F1635678', uid => 8010, passwd => True}
    account::useraccount{'F1795224': kind => 'studenti', login=>'F1795224', uid => 8090, passwd => True}
    account::useraccount{'F1796721': kind => 'studenti', login=>'F1796721', uid => 8103, passwd => True}
    account::useraccount{'F1787406': kind => 'studenti', login=>'F1787406', uid => 8053, passwd => True}
    account::useraccount{'F1762407': kind => 'studenti', login=>'F1762407', uid => 8041, passwd => True}
    account::useraccount{'F1752649': kind => 'studenti', login=>'F1752649', uid => 8023, passwd => True}
    account::useraccount{'F1817507': kind => 'studenti', login=>'F1817507', uid => 8252, passwd => True}
    account::useraccount{'F1809139': kind => 'studenti', login=>'F1809139', uid => 8188, passwd => True}
    account::useraccount{'F1797009': kind => 'studenti', login=>'F1797009', uid => 8110, passwd => True}
    account::useraccount{'F1793330': kind => 'studenti', login=>'F1793330', uid => 8071, passwd => True}
    account::useraccount{'F1786909': kind => 'studenti', login=>'F1786909', uid => 8051, passwd => True}
    account::useraccount{'F1794867': kind => 'studenti', login=>'F1794867', uid => 8088, passwd => True}
    account::useraccount{'F1801277': kind => 'studenti', login=>'F1801277', uid => 8142, passwd => True}
    account::useraccount{'F1816777': kind => 'studenti', login=>'F1816777', uid => 8247, passwd => True}
    account::useraccount{'F1807899': kind => 'studenti', login=>'F1807899', uid => 8178, passwd => True}
    account::useraccount{'F1792639': kind => 'studenti', login=>'F1792639', uid => 8063, passwd => True}
    account::useraccount{'F1793891': kind => 'studenti', login=>'F1793891', uid => 8076, passwd => True}
    account::useraccount{'F1792619': kind => 'studenti', login=>'F1792619', uid => 8062, passwd => True}
    account::useraccount{'F1769999': kind => 'studenti', login=>'F1769999', uid => 8045, passwd => True}
    account::useraccount{'F1742996': kind => 'studenti', login=>'F1742996', uid => 8016, passwd => True}
    account::useraccount{'F1747873': kind => 'studenti', login=>'F1747873', uid => 8020, passwd => True}
    account::useraccount{'F1798521': kind => 'studenti', login=>'F1798521', uid => 8121, passwd => True}

  #TURNO B
    account::useraccount{'F1811642': kind => 'studenti', login=>'F1811642', uid => 8210, passwd => True}
    account::useraccount{'F1813672': kind => 'studenti', login=>'F1813672', uid => 8218, passwd => True}
    account::useraccount{'F1806275': kind => 'studenti', login=>'F1806275', uid => 8169, passwd => True}
    account::useraccount{'F1794077': kind => 'studenti', login=>'F1794077', uid => 8078, passwd => True}
    account::useraccount{'F1807315': kind => 'studenti', login=>'F1807315', uid => 8176, passwd => True}
    account::useraccount{'F1802759': kind => 'studenti', login=>'F1802759', uid => 8150, passwd => True}
    account::useraccount{'F1814102': kind => 'studenti', login=>'F1814102', uid => 8223, passwd => True}
    account::useraccount{'F1812064': kind => 'studenti', login=>'F1812064', uid => 8211, passwd => True}
    account::useraccount{'F1750112': kind => 'studenti', login=>'F1750112', uid => 8021, passwd => True}
    account::useraccount{'F1798966': kind => 'studenti', login=>'F1798966', uid => 8127, passwd => True}
    account::useraccount{'F1792970': kind => 'studenti', login=>'F1792970', uid => 8068, passwd => True}
    account::useraccount{'F1811056': kind => 'studenti', login=>'F1811056', uid => 8202, passwd => True}
    account::useraccount{'F1792646': kind => 'studenti', login=>'F1792646', uid => 8064, passwd => True}
    account::useraccount{'F1762085': kind => 'studenti', login=>'F1762085', uid => 8039, passwd => True}
    account::useraccount{'F1805501': kind => 'studenti', login=>'F1805501', uid => 8158, passwd => True}
    account::useraccount{'F1805351': kind => 'studenti', login=>'F1805351', uid => 8004, passwd => True}
    account::useraccount{'F1711349': kind => 'studenti', login=>'F1711349', uid => 8001, passwd => True}
    account::useraccount{'F1806072': kind => 'studenti', login=>'F1806072', uid => 8165, passwd => True}
    account::useraccount{'F1801239': kind => 'studenti', login=>'F1801239', uid => 8141, passwd => True}
    account::useraccount{'F1797058': kind => 'studenti', login=>'F1797058', uid => 8111, passwd => True}
    account::useraccount{'F1797153': kind => 'studenti', login=>'F1797153', uid => 8114, passwd => True}
    account::useraccount{'F1744359': kind => 'studenti', login=>'F1744359', uid => 8017, passwd => True}
    account::useraccount{'F1820146': kind => 'studenti', login=>'F1820146', uid => 8261, passwd => True}
    account::useraccount{'F1799750': kind => 'studenti', login=>'F1799750', uid => 8132, passwd => True}
    account::useraccount{'F1816303': kind => 'studenti', login=>'F1816303', uid => 8240, passwd => True}
    account::useraccount{'F1811319': kind => 'studenti', login=>'F1811319', uid => 8206, passwd => True}
    account::useraccount{'F1793341': kind => 'studenti', login=>'F1793341', uid => 8072, passwd => True}
    account::useraccount{'F1738207': kind => 'studenti', login=>'F1738207', uid => 8003, passwd => True}
    account::useraccount{'F1794622': kind => 'studenti', login=>'F1794622', uid => 8084, passwd => True}
    account::useraccount{'F1799188': kind => 'studenti', login=>'F1799188', uid => 8128, passwd => True}
    account::useraccount{'F1809359': kind => 'studenti', login=>'F1809359', uid => 8191, passwd => True}
    account::useraccount{'F1793049': kind => 'studenti', login=>'F1793049', uid => 8069, passwd => True}
    account::useraccount{'F1806155': kind => 'studenti', login=>'F1806155', uid => 8166, passwd => True}
    account::useraccount{'F1793490': kind => 'studenti', login=>'F1793490', uid => 8073, passwd => True}
    account::useraccount{'F1738075': kind => 'studenti', login=>'F1738075', uid => 8002, passwd => True}
    account::useraccount{'F1814564': kind => 'studenti', login=>'F1814564', uid => 8005, passwd => True}
    account::useraccount{'F1802393': kind => 'studenti', login=>'F1802393', uid => 8146, passwd => True}

  #TURNO C
    account::useraccount{'F1794038': kind => 'studenti', login=>'F1794038', uid => 8077, passwd => True}
    account::useraccount{'F1814690': kind => 'studenti', login=>'F1814690', uid => 8230, passwd => True}
    account::useraccount{'F1800838': kind => 'studenti', login=>'F1800838', uid => 8138, passwd => True}
    account::useraccount{'F1803118': kind => 'studenti', login=>'F1803118', uid => 8151, passwd => True}
    account::useraccount{'F1812579': kind => 'studenti', login=>'F1812579', uid => 8213, passwd => True}
    account::useraccount{'F1811114': kind => 'studenti', login=>'F1811114', uid => 8203, passwd => True}
    account::useraccount{'F1795508': kind => 'studenti', login=>'F1795508', uid => 8094, passwd => True}
    account::useraccount{'F1795968': kind => 'studenti', login=>'F1795968', uid => 8098, passwd => True}
    account::useraccount{'F1796144': kind => 'studenti', login=>'F1796144', uid => 8099, passwd => True}
    account::useraccount{'F1805748': kind => 'studenti', login=>'F1805748', uid => 8161, passwd => True}
    account::useraccount{'F1798116': kind => 'studenti', login=>'F1798116', uid => 8118, passwd => True}
    account::useraccount{'F1808868': kind => 'studenti', login=>'F1808868', uid => 8187, passwd => True}
    account::useraccount{'F1816330': kind => 'studenti', login=>'F1816330', uid => 8241, passwd => True}
    account::useraccount{'F1815558': kind => 'studenti', login=>'F1815558', uid => 8234, passwd => True}
    account::useraccount{'F1798810': kind => 'studenti', login=>'F1798810', uid => 8125, passwd => True}
    account::useraccount{'F1785971': kind => 'studenti', login=>'F1785971', uid => 8049, passwd => True}
    account::useraccount{'F1814545': kind => 'studenti', login=>'F1814545', uid => 8229, passwd => True}
    account::useraccount{'F1806551': kind => 'studenti', login=>'F1806551', uid => 8172, passwd => True}
    account::useraccount{'F1810254': kind => 'studenti', login=>'F1810254', uid => 8197, passwd => True}
    account::useraccount{'F1798581': kind => 'studenti', login=>'F1798581', uid => 8122, passwd => True}
    account::useraccount{'F1815766': kind => 'studenti', login=>'F1815766', uid => 8236, passwd => True}
    account::useraccount{'F1809360': kind => 'studenti', login=>'F1809360', uid => 8192, passwd => True}
    account::useraccount{'F1828180': kind => 'studenti', login=>'F1828180', uid => 8267, passwd => True}
    account::useraccount{'F1817340': kind => 'studenti', login=>'F1817340', uid => 8251, passwd => True}
    account::useraccount{'F1813156': kind => 'studenti', login=>'F1813156', uid => 8214, passwd => True}
    account::useraccount{'F1804558': kind => 'studenti', login=>'F1804558', uid => 8156, passwd => True}
    account::useraccount{'F1816679': kind => 'studenti', login=>'F1816679', uid => 8245, passwd => True}
    account::useraccount{'F1795925': kind => 'studenti', login=>'F1795925', uid => 8097, passwd => True}
    account::useraccount{'F1808256': kind => 'studenti', login=>'F1808256', uid => 8182, passwd => True}
    account::useraccount{'F1812406': kind => 'studenti', login=>'F1812406', uid => 8212, passwd => True}
    account::useraccount{'F1806241': kind => 'studenti', login=>'F1806241', uid => 8167, passwd => True}
    account::useraccount{'F1793703': kind => 'studenti', login=>'F1793703', uid => 8074, passwd => True}
    account::useraccount{'F1815821': kind => 'studenti', login=>'F1815821', uid => 8238, passwd => True}
    account::useraccount{'F1800761': kind => 'studenti', login=>'F1800761', uid => 8137, passwd => True}
    account::useraccount{'F1808247': kind => 'studenti', login=>'F1808247', uid => 8181, passwd => True}
    account::useraccount{'F1794400': kind => 'studenti', login=>'F1794400', uid => 8082, passwd => True}
    account::useraccount{'F1818408': kind => 'studenti', login=>'F1818408', uid => 8257, passwd => True}

  #TURNO D
    account::useraccount{'F1797117': kind => 'studenti', login=>'F1797117', uid => 8113, passwd => True}
    account::useraccount{'F1795152': kind => 'studenti', login=>'F1795152', uid => 8089, passwd => True}
    account::useraccount{'F1727820': kind => 'studenti', login=>'F1727820', uid => 8015, passwd => True}
    account::useraccount{'F1798370': kind => 'studenti', login=>'F1798370', uid => 8120, passwd => True}
    account::useraccount{'F1810864': kind => 'studenti', login=>'F1810864', uid => 8200, passwd => True}
    account::useraccount{'F1807493': kind => 'studenti', login=>'F1807493', uid => 8177, passwd => True}
    account::useraccount{'F1797065': kind => 'studenti', login=>'F1797065', uid => 8112, passwd => True}
    account::useraccount{'F1811419': kind => 'studenti', login=>'F1811419', uid => 8207, passwd => True}
    account::useraccount{'F1799888': kind => 'studenti', login=>'F1799888', uid => 8134, passwd => True}
    account::useraccount{'F1754596': kind => 'studenti', login=>'F1754596', uid => 8026, passwd => True}
    account::useraccount{'F1798592': kind => 'studenti', login=>'F1798592', uid => 8123, passwd => True}
    account::useraccount{'F1798227': kind => 'studenti', login=>'F1798227', uid => 8119, passwd => True}
    account::useraccount{'F1824734': kind => 'studenti', login=>'F1824734', uid => 8264, passwd => True}
    account::useraccount{'F1805919': kind => 'studenti', login=>'F1805919', uid => 8162, passwd => True}
    account::useraccount{'F1808606': kind => 'studenti', login=>'F1808606', uid => 8186, passwd => True}
    account::useraccount{'F1814295': kind => 'studenti', login=>'F1814295', uid => 8227, passwd => True}
    account::useraccount{'F1792658': kind => 'studenti', login=>'F1792658', uid => 8065, passwd => True}
    account::useraccount{'F1808473': kind => 'studenti', login=>'F1808473', uid => 8184, passwd => True}
    account::useraccount{'F1797000': kind => 'studenti', login=>'F1797000', uid => 8109, passwd => True}
    account::useraccount{'F1815790': kind => 'studenti', login=>'F1815790', uid => 8237, passwd => True}
    account::useraccount{'F1824800': kind => 'studenti', login=>'F1824800', uid => 8265, passwd => True}
    account::useraccount{'F1803511': kind => 'studenti', login=>'F1803511', uid => 8153, passwd => True}
    account::useraccount{'F1818212': kind => 'studenti', login=>'F1818212', uid => 8255, passwd => True}
    account::useraccount{'F1797678': kind => 'studenti', login=>'F1797678', uid => 8116, passwd => True}
    account::useraccount{'F1816696': kind => 'studenti', login=>'F1816696', uid => 8246, passwd => True}
    account::useraccount{'F1811493': kind => 'studenti', login=>'F1811493', uid => 8208, passwd => True}
    account::useraccount{'F1800687': kind => 'studenti', login=>'F1800687', uid => 8135, passwd => True}
    account::useraccount{'F1814169': kind => 'studenti', login=>'F1814169', uid => 8225, passwd => True}
    account::useraccount{'F1792925': kind => 'studenti', login=>'F1792925', uid => 8067, passwd => True}
    account::useraccount{'F1809235': kind => 'studenti', login=>'F1809235', uid => 8189, passwd => True}
    account::useraccount{'F1811527': kind => 'studenti', login=>'F1811527', uid => 8209, passwd => True}
    account::useraccount{'F1789575': kind => 'studenti', login=>'F1789575', uid => 8056, passwd => True}
    account::useraccount{'F1802322': kind => 'studenti', login=>'F1802322', uid => 8145, passwd => True}
    account::useraccount{'F1815718': kind => 'studenti', login=>'F1815718', uid => 8235, passwd => True}
    account::useraccount{'F1806256': kind => 'studenti', login=>'F1806256', uid => 8168, passwd => True}
    account::useraccount{'F1756065': kind => 'studenti', login=>'F1756065', uid => 8028, passwd => True}
    account::useraccount{'F1759892': kind => 'studenti', login=>'F1759892', uid => 8036, passwd => True}


  #RISERVE (Per abilitare, cambiare la shell da /sbin/nologin a /bin/bash
    account::useraccount{'F1805700': kind => 'studenti', login=>'F1805700', uid => 8160, passwd => True}
    account::useraccount{'F1762020': kind => 'studenti', login=>'F1762020', uid => 8038, passwd => True}
    account::useraccount{'F1817055': kind => 'studenti', login=>'F1817055', uid => 8248, passwd => True}
    account::useraccount{'F1599670': kind => 'studenti', login=>'F1599670', uid => 8009, passwd => True}
    account::useraccount{'F1808507': kind => 'studenti', login=>'F1808507', uid => 8185, passwd => True}
    account::useraccount{'F1664744': kind => 'studenti', login=>'F1664744', uid => 8013, passwd => True}
    account::useraccount{'F1753539': kind => 'studenti', login=>'F1753539', uid => 8025, passwd => True}
    account::useraccount{'F1825685': kind => 'studenti', login=>'F1825685', uid => 8266, passwd => True}
    account::useraccount{'F1796955': kind => 'studenti', login=>'F1796955', uid => 8108, passwd => True}
    account::useraccount{'F1798629': kind => 'studenti', login=>'F1798629', uid => 8124, passwd => True}
    account::useraccount{'F1756905': kind => 'studenti', login=>'F1756905', uid => 8031, passwd => True}
    account::useraccount{'F1758322': kind => 'studenti', login=>'F1758322', uid => 8033, passwd => True}
    account::useraccount{'F1797578': kind => 'studenti', login=>'F1797578', uid => 8115, passwd => True}
    account::useraccount{'F1795337': kind => 'studenti', login=>'F1795337', uid => 8091, passwd => True}
    account::useraccount{'F1798930': kind => 'studenti', login=>'F1798930', uid => 8126, passwd => True}
    account::useraccount{'F1745329': kind => 'studenti', login=>'F1745329', uid => 8019, passwd => True}
    account::useraccount{'F1806067': kind => 'studenti', login=>'F1806067', uid => 8164, passwd => True}
    account::useraccount{'F1796832': kind => 'studenti', login=>'F1796832', uid => 8106, passwd => True}
    account::useraccount{'F1807294': kind => 'studenti', login=>'F1807294', uid => 8175, passwd => True}
    account::useraccount{'F1333442': kind => 'studenti', login=>'F1333442', uid => 8007, passwd => True}
    account::useraccount{'F1472228': kind => 'studenti', login=>'F1472228', uid => 8008, passwd => True}
    account::useraccount{'F1818299': kind => 'studenti', login=>'F1818299', uid => 8256, passwd => True}
    account::useraccount{'F1814758': kind => 'studenti', login=>'F1814758', uid => 8231, passwd => True}
    account::useraccount{'F1810183': kind => 'studenti', login=>'F1810183', uid => 8196, passwd => True}
    account::useraccount{'F1794730': kind => 'studenti', login=>'F1794730', uid => 8086, passwd => True}
    account::useraccount{'F1796152': kind => 'studenti', login=>'F1796152', uid => 8100, passwd => True}
    account::useraccount{'F1810727': kind => 'studenti', login=>'F1810727', uid => 8199, passwd => True}
    account::useraccount{'F1806396': kind => 'studenti', login=>'F1806396', uid => 8170, passwd => True}
    account::useraccount{'F1816568': kind => 'studenti', login=>'F1816568', uid => 8243, passwd => True}
    account::useraccount{'F1756874': kind => 'studenti', login=>'F1756874', uid => 8030, passwd => True}
    account::useraccount{'F1795445': kind => 'studenti', login=>'F1795445', uid => 8092, passwd => True}
    account::useraccount{'F1794221': kind => 'studenti', login=>'F1794221', uid => 8080, passwd => True}
    account::useraccount{'F1757562': kind => 'studenti', login=>'F1757562', uid => 8032, passwd => True}
    account::useraccount{'F1800734': kind => 'studenti', login=>'F1800734', uid => 8136, passwd => True}
    account::useraccount{'F1767888': kind => 'studenti', login=>'F1767888', uid => 8044, passwd => True}
    account::useraccount{'F1808310': kind => 'studenti', login=>'F1808310', uid => 8183, passwd => True}
    account::useraccount{'F1807092': kind => 'studenti', login=>'F1807092', uid => 8174, passwd => True}
    account::useraccount{'F1799357': kind => 'studenti', login=>'F1799357', uid => 8130, passwd => True}
    account::useraccount{'F1814276': kind => 'studenti', login=>'F1814276', uid => 8226, passwd => True}
    account::useraccount{'F1792917': kind => 'studenti', login=>'F1792917', uid => 8066, passwd => True}
    account::useraccount{'F1808107': kind => 'studenti', login=>'F1808107', uid => 8180, passwd => True}
    account::useraccount{'F1752681': kind => 'studenti', login=>'F1752681', uid => 8024, passwd => True}
    account::useraccount{'F1758507': kind => 'studenti', login=>'F1758507', uid => 8034, passwd => True}
    account::useraccount{'F1762122': kind => 'studenti', login=>'F1762122', uid => 8040, passwd => True}
    account::useraccount{'F1797772': kind => 'studenti', login=>'F1797772', uid => 8117, passwd => True}
    account::useraccount{'F1817140': kind => 'studenti', login=>'F1817140', uid => 8249, passwd => True}
    account::useraccount{'F1760266': kind => 'studenti', login=>'F1760266', uid => 8037, passwd => True}
    account::useraccount{'F1787767': kind => 'studenti', login=>'F1787767', uid => 8055, passwd => True}
    account::useraccount{'F1819384': kind => 'studenti', login=>'F1819384', uid => 8260, passwd => True}
    account::useraccount{'F1150330': kind => 'studenti', login=>'F1150330', uid => 8006, passwd => True}
    account::useraccount{'F1817283': kind => 'studenti', login=>'F1817283', uid => 8250, passwd => True}
    account::useraccount{'F1813908': kind => 'studenti', login=>'F1813908', uid => 8221, passwd => True}
    account::useraccount{'F1795569': kind => 'studenti', login=>'F1795569', uid => 8095, passwd => True}
    account::useraccount{'F1803767': kind => 'studenti', login=>'F1803767', uid => 8154, passwd => True}
    account::useraccount{'F1814003': kind => 'studenti', login=>'F1814003', uid => 8222, passwd => True}
    account::useraccount{'F1802755': kind => 'studenti', login=>'F1802755', uid => 8149, passwd => True}
    account::useraccount{'F1819185': kind => 'studenti', login=>'F1819185', uid => 8259, passwd => True}
    account::useraccount{'F1795503': kind => 'studenti', login=>'F1795503', uid => 8093, passwd => True}
    account::useraccount{'F1795758': kind => 'studenti', login=>'F1795758', uid => 8096, passwd => True}
    account::useraccount{'F1799821': kind => 'studenti', login=>'F1799821', uid => 8133, passwd => True}
    account::useraccount{'F1758613': kind => 'studenti', login=>'F1758613', uid => 8035, passwd => True}
    account::useraccount{'F1814148': kind => 'studenti', login=>'F1814148', uid => 8224, passwd => True}
    account::useraccount{'F1794827': kind => 'studenti', login=>'F1794827', uid => 8087, passwd => True}
    account::useraccount{'F1817685': kind => 'studenti', login=>'F1817685', uid => 8253, passwd => True}
    account::useraccount{'F1646546': kind => 'studenti', login=>'F1646546', uid => 8011, passwd => True}
    account::useraccount{'F1803978': kind => 'studenti', login=>'F1803978', uid => 8155, passwd => True}
    account::useraccount{'F1813755': kind => 'studenti', login=>'F1813755', uid => 8219, passwd => True}
    account::useraccount{'F1805581': kind => 'studenti', login=>'F1805581', uid => 8159, passwd => True}
    account::useraccount{'F1801315': kind => 'studenti', login=>'F1801315', uid => 8143, passwd => True}
    account::useraccount{'F1794189': kind => 'studenti', login=>'F1794189', uid => 8079, passwd => True}
    account::useraccount{'F1810453': kind => 'studenti', login=>'F1810453', uid => 8198, passwd => True}
    account::useraccount{'F1784097': kind => 'studenti', login=>'F1784097', uid => 8047, passwd => True}
    account::useraccount{'F1786000': kind => 'studenti', login=>'F1786000', uid => 8050, passwd => True}
    account::useraccount{'F1822840': kind => 'studenti', login=>'F1822840', uid => 8262, passwd => True}
    account::useraccount{'F1764804': kind => 'studenti', login=>'F1764804', uid => 8042, passwd => True}
    account::useraccount{'F1799634': kind => 'studenti', login=>'F1799634', uid => 8131, passwd => True}
    account::useraccount{'F1796764': kind => 'studenti', login=>'F1796764', uid => 8105, passwd => True}
    account::useraccount{'F1705327': kind => 'studenti', login=>'F1705327', uid => 8014, passwd => True}
    account::useraccount{'F1806933': kind => 'studenti', login=>'F1806933', uid => 8173, passwd => True}
    account::useraccount{'F1750687': kind => 'studenti', login=>'F1750687', uid => 8022, passwd => True}
    account::useraccount{'F1787337': kind => 'studenti', login=>'F1787337', uid => 8052, passwd => True}
    account::useraccount{'F1783179': kind => 'studenti', login=>'F1783179', uid => 8046, passwd => True}
    account::useraccount{'F1794246': kind => 'studenti', login=>'F1794246', uid => 8081, passwd => True}
    account::useraccount{'F1796453': kind => 'studenti', login=>'F1796453', uid => 8102, passwd => True}
    account::useraccount{'F1875555': kind => 'studenti', login=>'F1875555', uid => 8268, passwd => True}
    account::useraccount{'F1815086': kind => 'studenti', login=>'F1815086', uid => 8232, passwd => True}
    account::useraccount{'F1809771': kind => 'studenti', login=>'F1809771', uid => 8195, passwd => True}
    account::useraccount{'F1793773': kind => 'studenti', login=>'F1793773', uid => 8075, passwd => True}
    account::useraccount{'F1816613': kind => 'studenti', login=>'F1816613', uid => 8244, passwd => True}
    account::useraccount{'F1805454': kind => 'studenti', login=>'F1805454', uid => 8157, passwd => True}
    account::useraccount{'F1796897': kind => 'studenti', login=>'F1796897', uid => 8107, passwd => True}
    account::useraccount{'F1794637': kind => 'studenti', login=>'F1794637', uid => 8085, passwd => True}
    account::useraccount{'F1809343': kind => 'studenti', login=>'F1809343', uid => 8190, passwd => True}
    account::useraccount{'F1793203': kind => 'studenti', login=>'F1793203', uid => 8070, passwd => True}
    account::useraccount{'F1801093': kind => 'studenti', login=>'F1801093', uid => 8140, passwd => True}
    account::useraccount{'F1800949': kind => 'studenti', login=>'F1800949', uid => 8139, passwd => True}
    account::useraccount{'F1794474': kind => 'studenti', login=>'F1794474', uid => 8083, passwd => True}
    account::useraccount{'F1792183': kind => 'studenti', login=>'F1792183', uid => 8060, passwd => True}
    account::useraccount{'F1809681': kind => 'studenti', login=>'F1809681', uid => 8194, passwd => True}
    account::useraccount{'F1744576': kind => 'studenti', login=>'F1744576', uid => 8018, passwd => True}
    account::useraccount{'F1814404': kind => 'studenti', login=>'F1814404', uid => 8228, passwd => True}
    account::useraccount{'F1787747': kind => 'studenti', login=>'F1787747', uid => 8054, passwd => True}
    account::useraccount{'F1805927': kind => 'studenti', login=>'F1805927', uid => 8163, passwd => True}
    account::useraccount{'F1791901': kind => 'studenti', login=>'F1791901', uid => 8058, passwd => True}
    account::useraccount{'F1806505': kind => 'studenti', login=>'F1806505', uid => 8171, passwd => True}
    account::useraccount{'F1813846': kind => 'studenti', login=>'F1813846', uid => 8220, passwd => True}
    account::useraccount{'F1756471': kind => 'studenti', login=>'F1756471', uid => 8029, passwd => True}
    account::useraccount{'F1791930': kind => 'studenti', login=>'F1791930', uid => 8059, passwd => True}
    account::useraccount{'F1796729': kind => 'studenti', login=>'F1796729', uid => 8104, passwd => True}
    account::useraccount{'F1816009': kind => 'studenti', login=>'F1816009', uid => 8239, passwd => True}
    account::useraccount{'F1823749': kind => 'studenti', login=>'F1823749', uid => 8263, passwd => True}
    account::useraccount{'F1813445': kind => 'studenti', login=>'F1813445', uid => 8216, passwd => True}
    account::useraccount{'F1818898': kind => 'studenti', login=>'F1818898', uid => 8258, passwd => True}
    account::useraccount{'F1802296': kind => 'studenti', login=>'F1802296', uid => 8144, passwd => True}
    account::useraccount{'F1802466': kind => 'studenti', login=>'F1802466', uid => 8147, passwd => True}
    account::useraccount{'F1810906': kind => 'studenti', login=>'F1810906', uid => 8201, passwd => True}
    account::useraccount{'F1811191': kind => 'studenti', login=>'F1811191', uid => 8204, passwd => True}
    account::useraccount{'F1815328': kind => 'studenti', login=>'F1815328', uid => 8233, passwd => True}
    account::useraccount{'F1767287': kind => 'studenti', login=>'F1767287', uid => 8043, passwd => True}
    account::useraccount{'F1817794': kind => 'studenti', login=>'F1817794', uid => 8254, passwd => True}
    account::useraccount{'F1803126': kind => 'studenti', login=>'F1803126', uid => 8152, passwd => True}
    account::useraccount{'F1807997': kind => 'studenti', login=>'F1807997', uid => 8179, passwd => True}

  # Laboratorio di Fisica Subnucleare (Veneziano)
    account::useraccount{'lfs00': kind => 'studenti', login=>'lfs00', uid => 5482, passwd => True}
    account::useraccount{'lfs01': kind => 'studenti', login=>'lfs01', uid => 5483, passwd => True}
    account::useraccount{'lfs02': kind => 'studenti', login=>'lfs02', uid => 5484, passwd => True}
    account::useraccount{'lfs03': kind => 'studenti', login=>'lfs03', uid => 5485, passwd => True}
    account::useraccount{'lfs04': kind => 'studenti', login=>'lfs04', uid => 5486, passwd => True}
    account::useraccount{'lfs05': kind => 'studenti', login=>'lfs05', uid => 5487, passwd => True}
    account::useraccount{'lfs06': kind => 'studenti', login=>'lfs06', uid => 5488, passwd => True}
    account::useraccount{'lfs07': kind => 'studenti', login=>'lfs07', uid => 5489, passwd => True}
    account::useraccount{'lfs08': kind => 'studenti', login=>'lfs08', uid => 5490, passwd => True}
    account::useraccount{'lfs09': kind => 'studenti', login=>'lfs09', uid => 5491, passwd => True}
    account::useraccount{'lfs10': kind => 'studenti', login=>'lfs10', uid => 5492, passwd => True}
    account::useraccount{'lfs11': kind => 'studenti', login=>'lfs11', uid => 5493, passwd => True}
    account::useraccount{'lfs12': kind => 'studenti', login=>'lfs12', uid => 5494, passwd => True}
    account::useraccount{'lfs13': kind => 'studenti', login=>'lfs13', uid => 5495, passwd => True}
    account::useraccount{'lfs14': kind => 'studenti', login=>'lfs14', uid => 5496, passwd => True}
    account::useraccount{'lfs15': kind => 'studenti', login=>'lfs15', uid => 5497, passwd => True}
    account::useraccount{'lfs16': kind => 'studenti', login=>'lfs16', uid => 5498, passwd => True}
    account::useraccount{'lfs17': kind => 'studenti', login=>'lfs17', uid => 5499, passwd => True}
    account::useraccount{'lfs18': kind => 'studenti', login=>'lfs18', uid => 5500, passwd => True}
    account::useraccount{'lfs19': kind => 'studenti', login=>'lfs19', uid => 5501, passwd => True}
    account::useraccount{'lfs20': kind => 'studenti', login=>'lfs20', uid => 5502, passwd => True}

  # Computational Condensed Matter Physics (Bachelet)
    account::useraccount{'fcm01': kind => 'studenti', login=>'fcm01', uid => 5503, passwd => True}
    account::useraccount{'fcm02': kind => 'studenti', login=>'fcm02', uid => 5504, passwd => True}
    account::useraccount{'fcm03': kind => 'studenti', login=>'fcm03', uid => 5505, passwd => True}
    account::useraccount{'fcm04': kind => 'studenti', login=>'fcm04', uid => 5506, passwd => True}
    account::useraccount{'fcm05': kind => 'studenti', login=>'fcm05', uid => 5507, passwd => True}
    account::useraccount{'fcm06': kind => 'studenti', login=>'fcm06', uid => 5508, passwd => True}
    account::useraccount{'fcm07': kind => 'studenti', login=>'fcm07', uid => 5509, passwd => True}
    account::useraccount{'fcm08': kind => 'studenti', login=>'fcm08', uid => 5510, passwd => True}
    account::useraccount{'fcm09': kind => 'studenti', login=>'fcm09', uid => 5511, passwd => True}
    account::useraccount{'fcm10': kind => 'studenti', login=>'fcm10', uid => 5512, passwd => True}
    account::useraccount{'fcm11': kind => 'studenti', login=>'fcm11', uid => 5513, passwd => True}
    account::useraccount{'fcm12': kind => 'studenti', login=>'fcm12', uid => 5514, passwd => True}
    account::useraccount{'fcm13': kind => 'studenti', login=>'fcm13', uid => 5515, passwd => True}
    account::useraccount{'fcm14': kind => 'studenti', login=>'fcm14', uid => 5516, passwd => True}
    account::useraccount{'fcm15': kind => 'studenti', login=>'fcm15', uid => 5517, passwd => True}
    account::useraccount{'fcm16': kind => 'studenti', login=>'fcm16', uid => 5518, passwd => True}
    account::useraccount{'fcm17': kind => 'studenti', login=>'fcm17', uid => 5519, passwd => True}
    account::useraccount{'fcm18': kind => 'studenti', login=>'fcm18', uid => 5520, passwd => True}
    account::useraccount{'fcm19': kind => 'studenti', login=>'fcm19', uid => 5521, passwd => True}
    account::useraccount{'fcm20': kind => 'studenti', login=>'fcm20', uid => 5522, passwd => True}
    account::useraccount{'fcm21': kind => 'studenti', login=>'fcm21', uid => 5523, passwd => True}
    account::useraccount{'fcm22': kind => 'studenti', login=>'fcm22', uid => 5524, passwd => True}
    account::useraccount{'fcm23': kind => 'studenti', login=>'fcm23', uid => 5525, passwd => True}
    account::useraccount{'fcm24': kind => 'studenti', login=>'fcm24', uid => 5526, passwd => True}
    account::useraccount{'fcm25': kind => 'studenti', login=>'fcm25', uid => 5527, passwd => True}
    account::useraccount{'fcm26': kind => 'studenti', login=>'fcm26', uid => 9621, passwd => True}
    account::useraccount{'fcm27': kind => 'studenti', login=>'fcm27', uid => 9622, passwd => True}
    account::useraccount{'fcm28': kind => 'studenti', login=>'fcm28', uid => 9623, passwd => True}
    account::useraccount{'fcm29': kind => 'studenti', login=>'fcm29', uid => 9624, passwd => True}
    account::useraccount{'fcm30': kind => 'studenti', login=>'fcm30', uid => 9625, passwd => True}
    account::useraccount{'fcm31': kind => 'studenti', login=>'fcm31', uid => 9626, passwd => True}
    account::useraccount{'fcm32': kind => 'studenti', login=>'fcm32', uid => 9627, passwd => True}
    account::useraccount{'fcm33': kind => 'studenti', login=>'fcm33', uid => 9628, passwd => True}
    account::useraccount{'fcm34': kind => 'studenti', login=>'fcm34', uid => 9629, passwd => True}
    account::useraccount{'fcm35': kind => 'studenti', login=>'fcm35', uid => 9630, passwd => True}

  # studenti di Nicoletta Gnan
    account::useraccount{'lcng01': kind => 'studenti', login=>'lcng01', uid => 5528, passwd => True}
    account::useraccount{'lcng02': kind => 'studenti', login=>'lcng02', uid => 5529, passwd => True}
    account::useraccount{'lcng03': kind => 'studenti', login=>'lcng03', uid => 5530, passwd => True}
    account::useraccount{'lcng04': kind => 'studenti', login=>'lcng04', uid => 5531, passwd => True}
    account::useraccount{'lcng05': kind => 'studenti', login=>'lcng05', uid => 5532, passwd => True}
    account::useraccount{'lcng06': kind => 'studenti', login=>'lcng06', uid => 5533, passwd => True}
    account::useraccount{'lcng07': kind => 'studenti', login=>'lcng07', uid => 5534, passwd => True}
    account::useraccount{'lcng08': kind => 'studenti', login=>'lcng08', uid => 5535, passwd => True}
    account::useraccount{'lcng09': kind => 'studenti', login=>'lcng09', uid => 5536, passwd => True}
    account::useraccount{'lcng10': kind => 'studenti', login=>'lcng10', uid => 5537, passwd => True}
    account::useraccount{'lcng11': kind => 'studenti', login=>'lcng11', uid => 5538, passwd => True}
    account::useraccount{'lcng12': kind => 'studenti', login=>'lcng12', uid => 5539, passwd => True}
    account::useraccount{'lcng13': kind => 'studenti', login=>'lcng13', uid => 5540, passwd => True}
    account::useraccount{'lcng14': kind => 'studenti', login=>'lcng14', uid => 5541, passwd => True}
    account::useraccount{'lcng15': kind => 'studenti', login=>'lcng15', uid => 5542, passwd => True}
    account::useraccount{'lcng16': kind => 'studenti', login=>'lcng16', uid => 5543, passwd => True}
    account::useraccount{'lcng17': kind => 'studenti', login=>'lcng17', uid => 5544, passwd => True}
    account::useraccount{'lcng18': kind => 'studenti', login=>'lcng18', uid => 5545, passwd => True}
    account::useraccount{'lcng19': kind => 'studenti', login=>'lcng19', uid => 5546, passwd => True}
    account::useraccount{'lcng20': kind => 'studenti', login=>'lcng20', uid => 5547, passwd => True}
    account::useraccount{'lcng21': kind => 'studenti', login=>'lcng21', uid => 5548, passwd => True}
    account::useraccount{'lcng22': kind => 'studenti', login=>'lcng22', uid => 5549, passwd => True}
    account::useraccount{'lcng23': kind => 'studenti', login=>'lcng23', uid => 5550, passwd => True}
    account::useraccount{'lcng24': kind => 'studenti', login=>'lcng24', uid => 5551, passwd => True}
    account::useraccount{'lcng25': kind => 'studenti', login=>'lcng25', uid => 5552, passwd => True}
    account::useraccount{'lcng26': kind => 'studenti', login=>'lcng26', uid => 5553, passwd => True}
    account::useraccount{'lcng27': kind => 'studenti', login=>'lcng27', uid => 5554, passwd => True}
    account::useraccount{'lcng28': kind => 'studenti', login=>'lcng28', uid => 5555, passwd => True}
    account::useraccount{'lcng29': kind => 'studenti', login=>'lcng29', uid => 5556, passwd => True}
    account::useraccount{'lcng30': kind => 'studenti', login=>'lcng30', uid => 5557, passwd => True}
    account::useraccount{'lcng31': kind => 'studenti', login=>'lcng31', uid => 5558, passwd => True}
    account::useraccount{'lcng32': kind => 'studenti', login=>'lcng32', uid => 5559, passwd => True}
    account::useraccount{'lcng33': kind => 'studenti', login=>'lcng33', uid => 5560, passwd => True}
    account::useraccount{'lcng34': kind => 'studenti', login=>'lcng34', uid => 5561, passwd => True}
    account::useraccount{'lcng35': kind => 'studenti', login=>'lcng35', uid => 5562, passwd => True}
    account::useraccount{'lcng36': kind => 'studenti', login=>'lcng36', uid => 5563, passwd => True}
    account::useraccount{'lcng37': kind => 'studenti', login=>'lcng37', uid => 5564, passwd => True}
    account::useraccount{'lcng38': kind => 'studenti', login=>'lcng38', uid => 5565, passwd => True}
    account::useraccount{'lcng39': kind => 'studenti', login=>'lcng39', uid => 5566, passwd => True}
    account::useraccount{'lcng40': kind => 'studenti', login=>'lcng40', uid => 5567, passwd => True}
    account::useraccount{'lcng41': kind => 'studenti', login=>'lcng41', uid => 5568, passwd => True}
    account::useraccount{'lcng42': kind => 'studenti', login=>'lcng42', uid => 5569, passwd => True}
    account::useraccount{'lcng43': kind => 'studenti', login=>'lcng43', uid => 5570, passwd => True}
    account::useraccount{'lcng44': kind => 'studenti', login=>'lcng44', uid => 5571, passwd => True}
    account::useraccount{'lcng45': kind => 'studenti', login=>'lcng45', uid => 5572, passwd => True}
    account::useraccount{'lcng46': kind => 'studenti', login=>'lcng46', uid => 5573, passwd => True}
    account::useraccount{'lcng47': kind => 'studenti', login=>'lcng47', uid => 5574, passwd => True}
    account::useraccount{'lcng48': kind => 'studenti', login=>'lcng48', uid => 5575, passwd => True}
    account::useraccount{'lcng49': kind => 'studenti', login=>'lcng49', uid => 5576, passwd => True}
    account::useraccount{'lcng50': kind => 'studenti', login=>'lcng50', uid => 5577, passwd => True}
    account::useraccount{'lcng51': kind => 'studenti', login=>'lcng51', uid => 5578, passwd => True}
    account::useraccount{'lcng52': kind => 'studenti', login=>'lcng52', uid => 5579, passwd => True}
    account::useraccount{'lcng53': kind => 'studenti', login=>'lcng53', uid => 5580, passwd => True}
    account::useraccount{'lcng54': kind => 'studenti', login=>'lcng54', uid => 5581, passwd => True}
    account::useraccount{'lcng55': kind => 'studenti', login=>'lcng55', uid => 5582, passwd => True}
    account::useraccount{'lcng56': kind => 'studenti', login=>'lcng56', uid => 5583, passwd => True}
    account::useraccount{'lcng57': kind => 'studenti', login=>'lcng57', uid => 5584, passwd => True}
    account::useraccount{'lcng58': kind => 'studenti', login=>'lcng58', uid => 5585, passwd => True}
    account::useraccount{'lcng59': kind => 'studenti', login=>'lcng59', uid => 5586, passwd => True}
    account::useraccount{'lcng60': kind => 'studenti', login=>'lcng60', uid => 5587, passwd => True}
    account::useraccount{'lcng61': kind => 'studenti', login=>'lcng61', uid => 5588, passwd => True}
    account::useraccount{'lcng62': kind => 'studenti', login=>'lcng62', uid => 5589, passwd => True}
    account::useraccount{'lcng63': kind => 'studenti', login=>'lcng63', uid => 5590, passwd => True}
    account::useraccount{'lcng64': kind => 'studenti', login=>'lcng64', uid => 5591, passwd => True}
    account::useraccount{'lcng65': kind => 'studenti', login=>'lcng65', uid => 5592, passwd => True}
    account::useraccount{'lcng66': kind => 'studenti', login=>'lcng66', uid => 5593, passwd => True}
    account::useraccount{'lcng67': kind => 'studenti', login=>'lcng67', uid => 5594, passwd => True}
    account::useraccount{'lcng68': kind => 'studenti', login=>'lcng68', uid => 5595, passwd => True}
    account::useraccount{'lcng69': kind => 'studenti', login=>'lcng69', uid => 5596, passwd => True}
    account::useraccount{'lcng70': kind => 'studenti', login=>'lcng70', uid => 5597, passwd => True}
    account::useraccount{'lcng71': kind => 'studenti', login=>'lcng71', uid => 5598, passwd => True}
    account::useraccount{'lcng72': kind => 'studenti', login=>'lcng72', uid => 5599, passwd => True}
    account::useraccount{'lcng73': kind => 'studenti', login=>'lcng73', uid => 5600, passwd => True}
    account::useraccount{'lcng74': kind => 'studenti', login=>'lcng74', uid => 5601, passwd => True}
    account::useraccount{'lcng75': kind => 'studenti', login=>'lcng75', uid => 5602, passwd => True}
    account::useraccount{'lcng76': kind => 'studenti', login=>'lcng76', uid => 5603, passwd => True}
    account::useraccount{'lcng77': kind => 'studenti', login=>'lcng77', uid => 5604, passwd => True}
    account::useraccount{'lcng78': kind => 'studenti', login=>'lcng78', uid => 5605, passwd => True}
    account::useraccount{'lcng79': kind => 'studenti', login=>'lcng79', uid => 5606, passwd => True}
    account::useraccount{'lcng80': kind => 'studenti', login=>'lcng80', uid => 5607, passwd => True}

  # studenti di Livia Soffi
    account::useraccount{'lcls01': kind => 'studenti', login=>'lcls01', uid => 5608, passwd => True}
    account::useraccount{'lcls02': kind => 'studenti', login=>'lcls02', uid => 5609, passwd => True}
    account::useraccount{'lcls03': kind => 'studenti', login=>'lcls03', uid => 5610, passwd => True}
    account::useraccount{'lcls04': kind => 'studenti', login=>'lcls04', uid => 5611, passwd => True}
    account::useraccount{'lcls05': kind => 'studenti', login=>'lcls05', uid => 5612, passwd => True}
    account::useraccount{'lcls06': kind => 'studenti', login=>'lcls06', uid => 5613, passwd => True}
    account::useraccount{'lcls07': kind => 'studenti', login=>'lcls07', uid => 5614, passwd => True}
    account::useraccount{'lcls08': kind => 'studenti', login=>'lcls08', uid => 5615, passwd => True}
    account::useraccount{'lcls09': kind => 'studenti', login=>'lcls09', uid => 5616, passwd => True}
    account::useraccount{'lcls10': kind => 'studenti', login=>'lcls10', uid => 5617, passwd => True}
    account::useraccount{'lcls11': kind => 'studenti', login=>'lcls11', uid => 5618, passwd => True}
    account::useraccount{'lcls12': kind => 'studenti', login=>'lcls12', uid => 5619, passwd => True}
    account::useraccount{'lcls13': kind => 'studenti', login=>'lcls13', uid => 5620, passwd => True}
    account::useraccount{'lcls14': kind => 'studenti', login=>'lcls14', uid => 5621, passwd => True}
    account::useraccount{'lcls15': kind => 'studenti', login=>'lcls15', uid => 5622, passwd => True}
    account::useraccount{'lcls16': kind => 'studenti', login=>'lcls16', uid => 5623, passwd => True}
    account::useraccount{'lcls17': kind => 'studenti', login=>'lcls17', uid => 5624, passwd => True}
    account::useraccount{'lcls18': kind => 'studenti', login=>'lcls18', uid => 5625, passwd => True}
    account::useraccount{'lcls19': kind => 'studenti', login=>'lcls19', uid => 5626, passwd => True}
    account::useraccount{'lcls20': kind => 'studenti', login=>'lcls20', uid => 5627, passwd => True}
    account::useraccount{'lcls21': kind => 'studenti', login=>'lcls21', uid => 5628, passwd => True}
    account::useraccount{'lcls22': kind => 'studenti', login=>'lcls22', uid => 5629, passwd => True}
    account::useraccount{'lcls23': kind => 'studenti', login=>'lcls23', uid => 5630, passwd => True}
    account::useraccount{'lcls24': kind => 'studenti', login=>'lcls24', uid => 5631, passwd => True}
    account::useraccount{'lcls25': kind => 'studenti', login=>'lcls25', uid => 5632, passwd => True}
    account::useraccount{'lcls26': kind => 'studenti', login=>'lcls26', uid => 5633, passwd => True}
    account::useraccount{'lcls27': kind => 'studenti', login=>'lcls27', uid => 5634, passwd => True}
    account::useraccount{'lcls28': kind => 'studenti', login=>'lcls28', uid => 5635, passwd => True}
    account::useraccount{'lcls29': kind => 'studenti', login=>'lcls29', uid => 5636, passwd => True}
    account::useraccount{'lcls30': kind => 'studenti', login=>'lcls30', uid => 5637, passwd => True}
    account::useraccount{'lcls31': kind => 'studenti', login=>'lcls31', uid => 5638, passwd => True}
    account::useraccount{'lcls32': kind => 'studenti', login=>'lcls32', uid => 5639, passwd => True}
    account::useraccount{'lcls33': kind => 'studenti', login=>'lcls33', uid => 5640, passwd => True}
    account::useraccount{'lcls34': kind => 'studenti', login=>'lcls34', uid => 5641, passwd => True}
    account::useraccount{'lcls35': kind => 'studenti', login=>'lcls35', uid => 5642, passwd => True}
    account::useraccount{'lcls36': kind => 'studenti', login=>'lcls36', uid => 5643, passwd => True}
    account::useraccount{'lcls37': kind => 'studenti', login=>'lcls37', uid => 5644, passwd => True}
    account::useraccount{'lcls38': kind => 'studenti', login=>'lcls38', uid => 5645, passwd => True}
    account::useraccount{'lcls39': kind => 'studenti', login=>'lcls39', uid => 5646, passwd => True}
    account::useraccount{'lcls40': kind => 'studenti', login=>'lcls40', uid => 5647, passwd => True}
    account::useraccount{'lcls41': kind => 'studenti', login=>'lcls41', uid => 5648, passwd => True}
    account::useraccount{'lcls42': kind => 'studenti', login=>'lcls42', uid => 5649, passwd => True}
    account::useraccount{'lcls43': kind => 'studenti', login=>'lcls43', uid => 5650, passwd => True}
    account::useraccount{'lcls44': kind => 'studenti', login=>'lcls44', uid => 5651, passwd => True}
    account::useraccount{'lcls45': kind => 'studenti', login=>'lcls45', uid => 5652, passwd => True}
    account::useraccount{'lcls46': kind => 'studenti', login=>'lcls46', uid => 5653, passwd => True}
    account::useraccount{'lcls47': kind => 'studenti', login=>'lcls47', uid => 5654, passwd => True}
    account::useraccount{'lcls48': kind => 'studenti', login=>'lcls48', uid => 5655, passwd => True}
    account::useraccount{'lcls49': kind => 'studenti', login=>'lcls49', uid => 5656, passwd => True}
    account::useraccount{'lcls50': kind => 'studenti', login=>'lcls50', uid => 5657, passwd => True}
    account::useraccount{'lcls51': kind => 'studenti', login=>'lcls51', uid => 5658, passwd => True}
    account::useraccount{'lcls52': kind => 'studenti', login=>'lcls52', uid => 5659, passwd => True}
    account::useraccount{'lcls53': kind => 'studenti', login=>'lcls53', uid => 5660, passwd => True}
    account::useraccount{'lcls54': kind => 'studenti', login=>'lcls54', uid => 5661, passwd => True}
    account::useraccount{'lcls55': kind => 'studenti', login=>'lcls55', uid => 5662, passwd => True}
    account::useraccount{'lcls56': kind => 'studenti', login=>'lcls56', uid => 5663, passwd => True}
    account::useraccount{'lcls57': kind => 'studenti', login=>'lcls57', uid => 5664, passwd => True}
    account::useraccount{'lcls58': kind => 'studenti', login=>'lcls58', uid => 5665, passwd => True}
    account::useraccount{'lcls59': kind => 'studenti', login=>'lcls59', uid => 5666, passwd => True}
    account::useraccount{'lcls60': kind => 'studenti', login=>'lcls60', uid => 5667, passwd => True}
    account::useraccount{'lcls61': kind => 'studenti', login=>'lcls61', uid => 5668, passwd => True}
    account::useraccount{'lcls62': kind => 'studenti', login=>'lcls62', uid => 5669, passwd => True}
    account::useraccount{'lcls63': kind => 'studenti', login=>'lcls63', uid => 5670, passwd => True}
    account::useraccount{'lcls64': kind => 'studenti', login=>'lcls64', uid => 5671, passwd => True}
    account::useraccount{'lcls65': kind => 'studenti', login=>'lcls65', uid => 5672, passwd => True}
    account::useraccount{'lcls66': kind => 'studenti', login=>'lcls66', uid => 5673, passwd => True}
    account::useraccount{'lcls67': kind => 'studenti', login=>'lcls67', uid => 5674, passwd => True}
    account::useraccount{'lcls68': kind => 'studenti', login=>'lcls68', uid => 5675, passwd => True}
    account::useraccount{'lcls69': kind => 'studenti', login=>'lcls69', uid => 5676, passwd => True}
    account::useraccount{'lcls70': kind => 'studenti', login=>'lcls70', uid => 5687, passwd => True}
    account::useraccount{'lcls71': kind => 'studenti', login=>'lcls71', uid => 5698, passwd => True}
    account::useraccount{'lcls72': kind => 'studenti', login=>'lcls72', uid => 5699, passwd => True}
    account::useraccount{'lcls73': kind => 'studenti', login=>'lcls73', uid => 5700, passwd => True}
    account::useraccount{'lcls74': kind => 'studenti', login=>'lcls74', uid => 5701, passwd => True}
    account::useraccount{'lcls75': kind => 'studenti', login=>'lcls75', uid => 5702, passwd => True}
    account::useraccount{'lcls76': kind => 'studenti', login=>'lcls76', uid => 5703, passwd => True}
    account::useraccount{'lcls77': kind => 'studenti', login=>'lcls77', uid => 5704, passwd => True}
    account::useraccount{'lcls78': kind => 'studenti', login=>'lcls78', uid => 5705, passwd => True}
    account::useraccount{'lcls79': kind => 'studenti', login=>'lcls79', uid => 5706, passwd => True}
    account::useraccount{'lcls80': kind => 'studenti', login=>'lcls80', uid => 5707, passwd => True}

  # studenti di Cristiano De Michele
    account::useraccount{'lcdm01': kind => 'studenti', login=>'lcdm01', uid => 5708, passwd => True}
    account::useraccount{'lcdm02': kind => 'studenti', login=>'lcdm02', uid => 5709, passwd => True}
    account::useraccount{'lcdm03': kind => 'studenti', login=>'lcdm03', uid => 5710, passwd => True}
    account::useraccount{'lcdm04': kind => 'studenti', login=>'lcdm04', uid => 5711, passwd => True}
    account::useraccount{'lcdm05': kind => 'studenti', login=>'lcdm05', uid => 5712, passwd => True}
    account::useraccount{'lcdm06': kind => 'studenti', login=>'lcdm06', uid => 5713, passwd => True}
    account::useraccount{'lcdm07': kind => 'studenti', login=>'lcdm07', uid => 5714, passwd => True}
    account::useraccount{'lcdm08': kind => 'studenti', login=>'lcdm08', uid => 5715, passwd => True}
    account::useraccount{'lcdm09': kind => 'studenti', login=>'lcdm09', uid => 5716, passwd => True}
    account::useraccount{'lcdm10': kind => 'studenti', login=>'lcdm10', uid => 5717, passwd => True}
    account::useraccount{'lcdm11': kind => 'studenti', login=>'lcdm11', uid => 5718, passwd => True}
    account::useraccount{'lcdm12': kind => 'studenti', login=>'lcdm12', uid => 5719, passwd => True}
    account::useraccount{'lcdm13': kind => 'studenti', login=>'lcdm13', uid => 5720, passwd => True}
    account::useraccount{'lcdm14': kind => 'studenti', login=>'lcdm14', uid => 5721, passwd => True}
    account::useraccount{'lcdm15': kind => 'studenti', login=>'lcdm15', uid => 5722, passwd => True}
    account::useraccount{'lcdm16': kind => 'studenti', login=>'lcdm16', uid => 5723, passwd => True}
    account::useraccount{'lcdm17': kind => 'studenti', login=>'lcdm17', uid => 5724, passwd => True}
    account::useraccount{'lcdm18': kind => 'studenti', login=>'lcdm18', uid => 5725, passwd => True}
    account::useraccount{'lcdm19': kind => 'studenti', login=>'lcdm19', uid => 5726, passwd => True}
    account::useraccount{'lcdm20': kind => 'studenti', login=>'lcdm20', uid => 5727, passwd => True}
    account::useraccount{'lcdm21': kind => 'studenti', login=>'lcdm21', uid => 5728, passwd => True}
    account::useraccount{'lcdm22': kind => 'studenti', login=>'lcdm22', uid => 5729, passwd => True}
    account::useraccount{'lcdm23': kind => 'studenti', login=>'lcdm23', uid => 5730, passwd => True}
    account::useraccount{'lcdm24': kind => 'studenti', login=>'lcdm24', uid => 5731, passwd => True}
    account::useraccount{'lcdm25': kind => 'studenti', login=>'lcdm25', uid => 5732, passwd => True}
    account::useraccount{'lcdm26': kind => 'studenti', login=>'lcdm26', uid => 5733, passwd => True}
    account::useraccount{'lcdm27': kind => 'studenti', login=>'lcdm27', uid => 5734, passwd => True}
    account::useraccount{'lcdm28': kind => 'studenti', login=>'lcdm28', uid => 5735, passwd => True}
    account::useraccount{'lcdm29': kind => 'studenti', login=>'lcdm29', uid => 5736, passwd => True}
    account::useraccount{'lcdm30': kind => 'studenti', login=>'lcdm30', uid => 5737, passwd => True}
    account::useraccount{'lcdm31': kind => 'studenti', login=>'lcdm31', uid => 5738, passwd => True}
    account::useraccount{'lcdm32': kind => 'studenti', login=>'lcdm32', uid => 5739, passwd => True}
    account::useraccount{'lcdm33': kind => 'studenti', login=>'lcdm33', uid => 5740, passwd => True}
    account::useraccount{'lcdm34': kind => 'studenti', login=>'lcdm34', uid => 5741, passwd => True}
    account::useraccount{'lcdm35': kind => 'studenti', login=>'lcdm35', uid => 5742, passwd => True}
    account::useraccount{'lcdm36': kind => 'studenti', login=>'lcdm36', uid => 5743, passwd => True}
    account::useraccount{'lcdm37': kind => 'studenti', login=>'lcdm37', uid => 5744, passwd => True}
    account::useraccount{'lcdm38': kind => 'studenti', login=>'lcdm38', uid => 5745, passwd => True}
    account::useraccount{'lcdm39': kind => 'studenti', login=>'lcdm39', uid => 5746, passwd => True}
    account::useraccount{'lcdm40': kind => 'studenti', login=>'lcdm40', uid => 5747, passwd => True}
    account::useraccount{'lcdm41': kind => 'studenti', login=>'lcdm41', uid => 5748, passwd => True}
    account::useraccount{'lcdm42': kind => 'studenti', login=>'lcdm42', uid => 5749, passwd => True}
    account::useraccount{'lcdm43': kind => 'studenti', login=>'lcdm43', uid => 5750, passwd => True}
    account::useraccount{'lcdm44': kind => 'studenti', login=>'lcdm44', uid => 5751, passwd => True}
    account::useraccount{'lcdm45': kind => 'studenti', login=>'lcdm45', uid => 5752, passwd => True}
    account::useraccount{'lcdm46': kind => 'studenti', login=>'lcdm46', uid => 5753, passwd => True}
    account::useraccount{'lcdm47': kind => 'studenti', login=>'lcdm47', uid => 5754, passwd => True}
    account::useraccount{'lcdm48': kind => 'studenti', login=>'lcdm48', uid => 5755, passwd => True}
    account::useraccount{'lcdm49': kind => 'studenti', login=>'lcdm49', uid => 5756, passwd => True}
    account::useraccount{'lcdm50': kind => 'studenti', login=>'lcdm50', uid => 5757, passwd => True}
    account::useraccount{'lcdm51': kind => 'studenti', login=>'lcdm51', uid => 5758, passwd => True}
    account::useraccount{'lcdm52': kind => 'studenti', login=>'lcdm52', uid => 5759, passwd => True}
    account::useraccount{'lcdm53': kind => 'studenti', login=>'lcdm53', uid => 5760, passwd => True}
    account::useraccount{'lcdm54': kind => 'studenti', login=>'lcdm54', uid => 5761, passwd => True}
    account::useraccount{'lcdm55': kind => 'studenti', login=>'lcdm55', uid => 5762, passwd => True}
    account::useraccount{'lcdm56': kind => 'studenti', login=>'lcdm56', uid => 5763, passwd => True}
    account::useraccount{'lcdm57': kind => 'studenti', login=>'lcdm57', uid => 5764, passwd => True}
    account::useraccount{'lcdm58': kind => 'studenti', login=>'lcdm58', uid => 5765, passwd => True}
    account::useraccount{'lcdm59': kind => 'studenti', login=>'lcdm59', uid => 5766, passwd => True}
    account::useraccount{'lcdm60': kind => 'studenti', login=>'lcdm60', uid => 5767, passwd => True}
    account::useraccount{'lcdm61': kind => 'studenti', login=>'lcdm61', uid => 5768, passwd => True}
    account::useraccount{'lcdm62': kind => 'studenti', login=>'lcdm62', uid => 5769, passwd => True}
    account::useraccount{'lcdm63': kind => 'studenti', login=>'lcdm63', uid => 5770, passwd => True}
    account::useraccount{'lcdm64': kind => 'studenti', login=>'lcdm64', uid => 5771, passwd => True}
    account::useraccount{'lcdm65': kind => 'studenti', login=>'lcdm65', uid => 5772, passwd => True}
    account::useraccount{'lcdm66': kind => 'studenti', login=>'lcdm66', uid => 5773, passwd => True}
    account::useraccount{'lcdm67': kind => 'studenti', login=>'lcdm67', uid => 5774, passwd => True}
    account::useraccount{'lcdm68': kind => 'studenti', login=>'lcdm68', uid => 5775, passwd => True}
    account::useraccount{'lcdm69': kind => 'studenti', login=>'lcdm69', uid => 5776, passwd => True}
    account::useraccount{'lcdm70': kind => 'studenti', login=>'lcdm70', uid => 5787, passwd => True}
    account::useraccount{'lcdm71': kind => 'studenti', login=>'lcdm71', uid => 5798, passwd => True}
    account::useraccount{'lcdm72': kind => 'studenti', login=>'lcdm72', uid => 5799, passwd => True}
    account::useraccount{'lcdm73': kind => 'studenti', login=>'lcdm73', uid => 5800, passwd => True}
    account::useraccount{'lcdm74': kind => 'studenti', login=>'lcdm74', uid => 5801, passwd => True}
    account::useraccount{'lcdm75': kind => 'studenti', login=>'lcdm75', uid => 5802, passwd => True}
    account::useraccount{'lcdm76': kind => 'studenti', login=>'lcdm76', uid => 5803, passwd => True}
    account::useraccount{'lcdm77': kind => 'studenti', login=>'lcdm77', uid => 5804, passwd => True}
    account::useraccount{'lcdm78': kind => 'studenti', login=>'lcdm78', uid => 5805, passwd => True}
    account::useraccount{'lcdm79': kind => 'studenti', login=>'lcdm79', uid => 5806, passwd => True}
    account::useraccount{'lcdm80': kind => 'studenti', login=>'lcdm80', uid => 5807, passwd => True}

  # studenti di computing methods (Rahatlou)
    account::useraccount{'cmp01': kind => 'studenti', login=>'cmp01', uid => 9631, passwd => True}  
    account::useraccount{'cmp02': kind => 'studenti', login=>'cmp02', uid => 9632, passwd => True}  
    account::useraccount{'cmp03': kind => 'studenti', login=>'cmp03', uid => 9633, passwd => True}  
    account::useraccount{'cmp04': kind => 'studenti', login=>'cmp04', uid => 9634, passwd => True}  
    account::useraccount{'cmp05': kind => 'studenti', login=>'cmp05', uid => 9635, passwd => True}  
    account::useraccount{'cmp06': kind => 'studenti', login=>'cmp06', uid => 9636, passwd => True}  
    account::useraccount{'cmp07': kind => 'studenti', login=>'cmp07', uid => 9637, passwd => True}  
    account::useraccount{'cmp08': kind => 'studenti', login=>'cmp08', uid => 9638, passwd => True}  
    account::useraccount{'cmp09': kind => 'studenti', login=>'cmp09', uid => 9639, passwd => True}  
    account::useraccount{'cmp10': kind => 'studenti', login=>'cmp10', uid => 9640, passwd => True}  
    account::useraccount{'cmp11': kind => 'studenti', login=>'cmp11', uid => 9641, passwd => True}  
    account::useraccount{'cmp12': kind => 'studenti', login=>'cmp12', uid => 9642, passwd => True}  
    account::useraccount{'cmp13': kind => 'studenti', login=>'cmp13', uid => 9643, passwd => True}  
    account::useraccount{'cmp14': kind => 'studenti', login=>'cmp14', uid => 9644, passwd => True}  
    account::useraccount{'cmp15': kind => 'studenti', login=>'cmp15', uid => 9645, passwd => True}  
    account::useraccount{'cmp16': kind => 'studenti', login=>'cmp16', uid => 9646, passwd => True}  
    account::useraccount{'cmp17': kind => 'studenti', login=>'cmp17', uid => 9647, passwd => True}  
    account::useraccount{'cmp18': kind => 'studenti', login=>'cmp18', uid => 9648, passwd => True}  
    account::useraccount{'cmp19': kind => 'studenti', login=>'cmp19', uid => 9649, passwd => True}  
    account::useraccount{'cmp20': kind => 'studenti', login=>'cmp20', uid => 9650, passwd => True}  
    account::useraccount{'cmp21': kind => 'studenti', login=>'cmp21', uid => 9651, passwd => True}  
    account::useraccount{'cmp22': kind => 'studenti', login=>'cmp22', uid => 9652, passwd => True}  
    account::useraccount{'cmp23': kind => 'studenti', login=>'cmp23', uid => 9653, passwd => True}  
    account::useraccount{'cmp24': kind => 'studenti', login=>'cmp24', uid => 9654, passwd => True}  
    account::useraccount{'cmp25': kind => 'studenti', login=>'cmp25', uid => 9655, passwd => True}  
    account::useraccount{'cmp26': kind => 'studenti', login=>'cmp26', uid => 9656, passwd => True}  
    account::useraccount{'cmp27': kind => 'studenti', login=>'cmp27', uid => 9657, passwd => True}  
    account::useraccount{'cmp28': kind => 'studenti', login=>'cmp28', uid => 9658, passwd => True}  
    account::useraccount{'cmp29': kind => 'studenti', login=>'cmp29', uid => 9659, passwd => True}  
    account::useraccount{'cmp30': kind => 'studenti', login=>'cmp30', uid => 9660, passwd => True}  
    account::useraccount{'cmp31': kind => 'studenti', login=>'cmp31', uid => 9661, passwd => True}  
    account::useraccount{'cmp32': kind => 'studenti', login=>'cmp32', uid => 9662, passwd => True}  
    account::useraccount{'cmp33': kind => 'studenti', login=>'cmp33', uid => 9663, passwd => True}  
    account::useraccount{'cmp34': kind => 'studenti', login=>'cmp34', uid => 9664, passwd => True}  
    account::useraccount{'cmp45': kind => 'studenti', login=>'cmp35', uid => 9665, passwd => True}  
    account::useraccount{'cmp36': kind => 'studenti', login=>'cmp36', uid => 9666, passwd => True}  
    account::useraccount{'cmp37': kind => 'studenti', login=>'cmp37', uid => 9667, passwd => True}  
    account::useraccount{'cmp38': kind => 'studenti', login=>'cmp38', uid => 9668, passwd => True}  
    account::useraccount{'cmp39': kind => 'studenti', login=>'cmp39', uid => 9669, passwd => True}  
    account::useraccount{'cmp40': kind => 'studenti', login=>'cmp40', uid => 9670, passwd => True}  
}
