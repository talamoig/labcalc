define account::useraccount($kind,$login,$uid,$ensure = "present", $passwd = False, $defshell = "/bin/bash", $defperms = 775){
  if ($passwd == True and $ensure == "present") {
    $pass_notify=[ Exec['nisupdate'],Exec["${login}-passwd"]]
  }
  else {
    $pass_notify=[Exec['nisupdate']]
  }
  user{$login:
    shell => $defshell,
    home => "/home/$kind/$login",
    ensure => $ensure,
    uid => "$uid",
    gid => "$kind",
    groups => dialout,
    managehome => true,
    notify => $pass_notify
    }

  if ($kind == 'studenti') {
    file{"/home/studenti/$login":
      ensure => directory,
      group => studenti,
      mode => $defperms,
      require => User["$login"],
    }
    file{"/home/studenti/$login/.config":
      ensure => directory,
      group => studenti,
      mode => $defperms,
      require => User["$login"],
    }
    file{"/home/studenti/$login/.config/pcmanfm":
      ensure => directory,
      group => studenti,
      mode => $defperms,
      require => User["$login"],
    }
    file{"/home/studenti/$login/.config/pcmanfm/LXDE":
      ensure => directory,
      group => studenti,
      mode => $defperms,
      require => User["$login"],
    }
    file{"/home/studenti/$login/.emacs":
      source => "puppet:///modules/account/emacs.template",
      ensure => present,
      owner => "$login",
      group => studenti
    }
    file{"/home/studenti/$login/.config/pcmanfm/LXDE/desktop-items-0.conf":
      source => "puppet:///modules/account/desktop-items-0.conf",
      ensure => present,
      owner => "$login",
      group => studenti
    }
  }
    
  if ($kind == 'esami') {
    file{"/home/esami/$login":
      ensure => directory,
      group => esami,
      owner => "$login",
      mode => 700,
      require => User["$login"],
    }
  }
  
  if ($passwd == True) {
    exec{"${login}-passwd":
      command => "/bin/echo $login | passwd --stdin $login",
      require => User[$login],
      refreshonly => true,
    }
  }
  
}
