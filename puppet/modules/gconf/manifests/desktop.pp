class gconf::desktop{

  file{"/etc/gconf/gconf.xml.mandatory":
    ensure => present,
    recurse => true,
    source => "puppet:///modules/gconf/gconf.xml.mandatory",
  }
  
}
