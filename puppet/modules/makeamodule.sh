#!/bin/bash

if [ $# -ne 1 ]
then
echo usage: $0 modulename
exit
fi
mkdir -p "$1"/{files,manifests,lib,templates}
touch "$1"/manifests/init.pp
