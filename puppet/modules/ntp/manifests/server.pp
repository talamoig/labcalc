class ntp::server{
  package{"ntpdate":
    ensure => installed
  }

  file{"/etc/ntp.conf":
    source => "puppet:///modules/ntp/ntp.conf",
    notify => Service["ntpd"]
  }

  service{"ntpd":
    enable => true,
    ensure => running,
    require => Package["ntpdate"]
  }
}
