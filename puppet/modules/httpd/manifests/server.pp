class httpd::server{
  package{"httpd":
    ensure => installed
  }

  file{"/etc/httpd/conf.d/repo.conf":
    source => "puppet:///modules/httpd/repo.conf",
    notify => Service["httpd"]
  }

  file{"/etc/httpd/conf/httpd.conf":
    source => "puppet:///modules/httpd/httpd.conf",
    notify => Service["httpd"]
  }

  file{"/etc/httpd/conf.d/homes-studenti.conf":
    source => "puppet:///modules/httpd/homes-studenti.conf",
    notify => Service["httpd"]
  }

  service{"httpd":
    enable => true,
    ensure => running,
    require => Package["httpd"]
  }
}
