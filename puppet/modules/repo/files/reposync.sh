#!/bin/sh

mirror="mirror.crazynetwork.it/ScientificLinux"
repobase="/home/install/repo/scientificlinux"

rsync -vart rsync://${mirror}/6.5/x86_64/os/ ${repobase}/6.5/x86_64/os
if [ $? -ne 0 ]; then
echo "ERROR getting os files from ${mirror} for ${vers} x64, quitting."
exit 1
fi
rsync -vart rsync://${mirror}/6.5/x86_64/updates/ ${repobase}/6.5/x86_64/updates
if [ $? -ne 0 ]; then
echo "ERROR getting updates files from ${mirror} for ${vers} x64, quitting."
exit 1
fi

rsync -vart rsync://${mirror}/6.5/i386/os/ ${repobase}/6.5/i386/os
if [ $? -ne 0 ]; then
echo "ERROR getting os files from ${mirror} for ${vers} x64, quitting."
exit 1
fi
rsync -vart rsync://${mirror}/6.5/i386/updates/ ${repobase}/6.5/i386/updates
if [ $? -ne 0 ]; then
echo "ERROR getting updates files from ${mirror} for ${vers} x64, quitting."
exit 1
fi
