class repo::server{
  file{"/root/reposync.sh":
    ensure => present,
    source => "puppet:///modules/repo/reposync.sh",
    mode => 755,
  }

  cron{"reposync":
    user => root,
    hour => "2",
    minute => "0",
    command => "/root/reposync.sh",
    require => File["/root/reposync.sh"],
  }
  
}
