class repo::client{
  file{'/etc/yum.repos.d/sl.repo':
    ensure => present,
    source => "puppet:///modules/repo/sl.repo",    
  }
}
