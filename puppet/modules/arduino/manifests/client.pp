class arduino::client{

  file{"/usr/local/bin/arduino":
        source => "puppet:///modules/arduino/arduino",
        mode => "755"
  }

}
