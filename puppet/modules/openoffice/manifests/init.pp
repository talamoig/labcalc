class openoffice {
  exec{'install-openoffice-rpms':
    command => '/bin/rpm -ivh /home/install/rpms/OpenOffice-4.1.1/en-US/RPMS/*rpm /home/install/rpms/OpenOffice-4.1.1/en-US/RPMS/desktop-integration/openoffice4.1.1-redhat-menus-4.1.1-9775.noarch.rpm',
    creates => '/usr/bin/openoffice4',
  }
}
