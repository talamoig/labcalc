class ssh::server{

  package{"openssh-server": ensure => present}

  service{"sshd":
    ensure => running,    
  }

  file{"/etc/ssh/sshd_config":
    source => "puppet:///modules/ssh/sshd_config",
    notify => Service["sshd"]
  }

}
