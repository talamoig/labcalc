class tftp::server{
  package{"tftp-server": ensure => installed}

#  file{"/var/lib/tftpboot/sl6":
#    ensure => directory,
#  }

#  file{"/var/lib/tftpboot/sl6/x86_64":
#    ensure => directory,
#    require => File["/var/lib/tftpboot/sl6/"]
#  }

#  file{"/var/lib/tftpboot/sl6/i386":
#    ensure => directory,
#    require => File["/var/lib/tftpboot/sl6/"]
#  }

#  file{"/var/lib/tftpboot/sl6/i386/initrd.img":
#    source => "puppet:///modules/tftp/i386/initrd.img",
#    require => File["/var/lib/tftpboot/sl6/i386"]
#  }

#  file{"/var/lib/tftpboot/sl6/i386/vmlinuz":
#    source => "puppet:///modules/tftp/i386/vmlinuz",
#    require => File["/var/lib/tftpboot/sl6/i386"]
#  }

#  file{"/var/lib/tftpboot/sl6/x86_64/initrd.img":
#    source => "puppet:///modules/tftp/x86_64/initrd.img",
#    require => File["/var/lib/tftpboot/sl6/x86_64"]
#  }
  
#  file{"/var/lib/tftpboot/sl6/x86_64/vmlinuz":
#    source => "puppet:///modules/tftp/x86_64/vmlinuz",
#    require => File["/var/lib/tftpboot/sl6/x86_64"]
#  }

#file{"/var/lib/tftpboot/pxelinux.0":
#    ensure => present,
#    source => "puppet:///modules/tftp/pxelinux.0",
#    require => File["/var/lib/tftpboot"],
#  }
  
#  file{"/var/lib/tftpboot/menu.c32":
#    ensure => present,
#    source => "puppet:///modules/tftp/menu.c32",
#    require => File["/var/lib/tftpboot"],
#  }
#  file{"/etc/xinetd.d/tftp":
#    source => "puppet:///modules/tftp/tftp",
#    require => Package["tftp-server"],
#    notify => Service["xinetd"]
#  }
  
#  service{"xinetd":
#    ensure => running,    
#  }

#  group{"tftpd":
#    ensure => present,
#    require => Package['tftp-server'],
#  }
#  user{"tftpd":
#    ensure => present,
#    gid => "tftpd",
#    require => Group['tftpd']
#  }

#  file{"/var/lib/tftpboot":
#    ensure => directory,
#    owner => 'tftpd',
#    group => 'tftpd',
#    require => User['tftpd'],
#  }
#  file{"/var/lib/tftpboot/pxelinux.cfg":
#    ensure => directory,
#    require => File["/var/lib/tftpboot"],
#  }
#  file{"/var/lib/tftpboot/pxelinux.cfg/default":
#    ensure => "present",
#    source => "puppet:///modules/tftp/pxelinux.cfg/default"
#  }
#  
#  file{"/home/install/ks/desktop.cfg":
#    ensure => "present",
#    source => "puppet:///modules/tftp/desktop.cfg",
#  }
}
