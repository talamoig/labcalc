class bind::server {

  package{"bind":
    ensure => installed
  }

  package{"bind-utils":
    ensure => installed
  }

  File{
    ensure => present,
    require => Package["bind"],
    notify => Service["named"]
  }

  file{"/etc/sysconfig/named": source => "puppet:///modules/bind/sysconfig.named",}
  file{"/etc/named.conf": source => "puppet:///modules/bind/named.conf",}
  file{"/etc/named.labcalc": source => "puppet:///modules/bind/named.labcalc",}
  file{"/var/named/1.168.192.zone": source => "puppet:///modules/bind/1.168.192.zone",}
  file{"/var/named/labcalc.hosts": source => "puppet:///modules/bind/labcalc.hosts",}
  
    service{"named":
      ensure => running,
      require => Package["bind"]}
    
}

