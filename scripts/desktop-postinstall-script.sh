#!/bin/bash

rm /etc/cron.daily/yum-autoupdate
rpm -ivh http://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-11.noarch.rpm
yum -y install puppet

cd /home/install/puppet
puppet apply --modulepath ./modules manifests/site.pp
