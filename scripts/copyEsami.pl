#!/usr/bin/perl

# needs ghostscript and enscript

use Getopt::Long;
use File::Basename;

$help;
$from = 1;
$to = 41;
$dir = 'esami';

GetOptions('help' => \$help, 'from=i' => \$from,
	   'to=i' => \$to, 'dir=s' => \$dir);

if ($help) {
    help();
    exit 0;
}

$cmd = "cp -r esami esami.old";
`$cmd`;
$cmd = "mkdir $dir";
`$cmd`;

for ($i = $from; $i <= $to; $i++){
    $cmd = "ssh pc$i /bin/find ~studente -type f -name \\\\*.c 2>/dev/null";
    @files = `$cmd`;
    foreach $f (@files) {
	chomp $f;
	$d = $f;
	$d =~ s/.*\///;
	$f =~ s/ /\\\\\\ /g;
	$f =~ s/</\\</g;
	$f =~ s/>/\\>/g;
	$d =~ s/</\\</g;
	$d =~ s/>/\\>/g;
	$prefix = "";
	if ($f =~ m/local\/studente\/.*\/.*/) {
	    $prefix = "-bad";
	}
	$d =~ s/\.c$//;
	$cmd = "scp pc$i:\"$f\" $dir/$d-pc$i$prefix.c\n";
	print "$cmd\n";
	`$cmd`;
    }
}

exit 0;

sub help() {
    print "copyEsami.pl [--from <f>] [--to <t>] [--dir <dirname>]\n";
    print "Get files from programs written after YYYYMMDD\n";
    print "options --from and --to allows to loop over part of the PCs.\n";
    print "Default values are 1 and 41.\n";
    print "Option --dir put files in directory <dirname>.\n\n";
#    print "To upload to Dropbox see http://xmodulo.com/access-dropbox-command-line-linux.html\n";
#    print "Use the command ./dropbox_uploader.sh upload *.pdf /.\n";
}
