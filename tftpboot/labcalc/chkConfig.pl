#!/usr/bin/perl

`service nis restart`;
print "=================================================================================================\n";
print "---- check packages: \n";
$cmd = `which gnuplot`;
if ($cmd !~ m/gnuplot/) {
    print " gnuplot missing *****************\n ";
} else {
    print $cmd;
}
print "---- check NIS functionality: \n";
print `ypcat passwd | tail -n 1`;
print "---- check ExoPlanets software: \n";
print `pip list --format=legacy | grep "\(scipy\|astropy\|matplotlib\|emcee\|PyTransit\)"`;
print `dpkg-query -W python-tk`;
print "---- NFS mounts: \n";
print `grep 192.168.1.254 /etc/fstab`;
print "---- check local account: \n";
print `ls /local/studente/Desktop`;
print "---- check crontab: \n";
print `grep shutdwon /etc/crontab`;
