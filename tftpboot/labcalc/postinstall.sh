#!/bin/sh

# TOOLS
apt-get install -y tftp
apt-get install -y less
apt-get install -y nfs-common
apt-get install -y gnuplot
apt-get install -y tcsh
apt-get install -y cmake
apt-get install -y qt4-default  qt4-dev-tools libxerces-c-dev

# NIS
cd /etc
echo "labcalc.fis" > defaultdomain
wget -O hosts http://192.168.1.254/labcalcConf/hosts
wget -O yp.conf http://192.168.1.254/labcalcConf/yp.conf
wget -O nsswitch.conf http://192.168.1.254/labcalcConf/nsswitch.conf
grep -v "+::::::" /etc/passwd > /etc/passwd.orig
echo "+::::::" >> /etc/passwd.orig
mv /etc/passwd.orig /etc/passwd
export DEBIAN_FRONTEND=noninteractive

# COMPILERS
apt-get install -y git
apt-get install -y ipython
apt-get install -y python-pip
apt-get install -y python-tk
pip install scipy 
pip install astropy
pip install matplotlib 
pip install emcee 
pip install numba
git clone https://github.com/hpparvi/PyTransit.git
cd PyTransit
python setup.py install

# FILESYSTEM
cd /etc
cat /etc/fstab | grep -v 192.168.1.254 > fstab.new
wget -O fstab.strip http://192.168.1.254/labcalcConf/fstab.strip
cat fstab.new fstab.strip > fstab
rm -f fstab.new fstab.strip
wget -O /etc/network/if-up.d/mount-nfs http://192.168.1.254/labcalcConf/mount-nfs
chmod 755 /etc/network/if-up.d/mount-nfs
mount -a

# SERVICES
apt-get install -y openssh-server
`/sbin/ifconfig | grep 192.168 | sed -e "s/^.*inet addr:192.168.1./hostname pc/" | sed -e "s/ \+Bcast.*//"`

# OTHER
mkdir -p /local/studente/Desktop
chown -R studente:studente /local/studente
usermod -d /local/studente studente
cd /local/studente/Desktop
wget -O menu-xdg-X-Debian-Applications-Terminal-Emulators-lxterminal.desktop http://192.168.1.254/labcalcConf/menu-xdg-X-Debian-Applications-Terminal-Emulators-lxterminal.desktop
chmod a-w /local/studente/Desktop/menu-xdg-X-Debian-Applications-Terminal-Emulators-lxterminal.desktop
find /local/studente/Desktop ! -name menu\* -exec rm -f {} \; 
cd /local/studente/.config/pcmanfm/LXDE
wget -O desktop-items-0.conf http://192.168.1.254/labcalcConf/desktop-items-0.conf

mkdir -p /root/.ssh
cd /root/.ssh
wget -O authorized_keys http://192.168.1.254/labcalcConf/authorized_keys

mkdir /usr/share/backgrounds
cd /usr/share/backgrounds
wget -O Sapienza.png http://192.168.1.254/labcalcConf/Sapienza.png
echo "#!/bin/sh" > /local/studente/.login
grep -v "pcmanfm --set-wallpaper" /etc/xdg/lxsession/LXDE/autostart > /etc/xdg/lxsession/LXDE/autostart.new
echo "@pcmanfm --set-wallpaper=\"/usr/share/backgrounds/Sapienza.png\"" >> /etc/xdg/lxsession/LXDE/autostart.new
mv /etc/xdg/lxsession/LXDE/autostart.new /etc/xdg/lxsession/LXDE/autostart

apt-get --purge -y remove wicd

cat /etc/crontab | grep -v shutdown > /etc/crontab.new
echo "0 20 * * * root /sbin/shutdown -h now" >> /etc/crontab.new
mv /etc/crontab.new /etc/crontab

# prepare for future updates
wget -O /etc/init.d/labcalc http://192.168.1.254/labcalcConf/labcalc
chmod 755 /etc/init.d/labcalc
update-rc.d labcalc defaults
